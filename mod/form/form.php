<?php 

class form extends mycelium_controller {
    public function index(){
        $det = $this->input->get("details");
        $dtl = explode("/",$det);
        echo $this->$dtl[0]($dtl); 
    }
    public function stat(){
        $this->load->rules("form");
        $this->load->model("aksi");
        $res = $this->db->get_stat();
        echo $res;
    }
    public function getsall(){
        $this->load->rules("form");
        $this->load->model("aksi");
        $res = $this->db->get_form3();
        echo $res;
    }
    public function gets(){
        $this->load->rules("form");
        $this->load->model("aksi");
        $res = $this->db->get_form();
        echo $res;
    }
    public function edits($dtl){
        $this->load->rules("form");
        $this->load->model("aksi");
        $res = $this->db->get_form4($dtl[1]);
        echo $res;
    }
    public function cari($dtl){
        $this->load->rules("form");
        $this->load->model("aksi");
        $res = $this->db->get_form2($dtl[1]);
        echo $res;
    }
    public function notif_email(){
        
        $this->load->library("PHPMailer");

        require "library/PHPMailer/class.smtp.php";
        require "library/PHPMailer/class.phpmaileroauth.php"; 

        $email = $this->PHPMailer();
        $email->SMTPDebug = 2;
        $email->isSMTP();                                      // Set mailer to use SMTP
        $email->Host = 'sumberadi.idwebhost.com ';              // Specify main and backup SMTP servers
        $email->SMTPAuth = true;                               // Enable SMTP authentication
        $email->Username = 'admin@rajendra-tours.com';                 // SMTP username
        $email->Password = '1234logmein';                           // SMTP password
        $email->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $email->Port = 465;                                    // TCP port to connect to
        
        $email->From = 'admin@rajendra-tours.com';
        $email->FromName = 'Layanan Email Rajendra';
        $email->addAddress("tes@gmail.com");     // Add a recipient
        //$email->addAddress("rahmat_sumber@yahoo.com");
        //$email->addAttachment('content.html');         // Add attachments
      //  $email->msgHTML(file_get_contents('content.html'), dirname(__FILE__));
        //$email->isHTML();
        
        $email->Subject = "Pemberitahuan tiket";
        $email->Body    = 'Pemberitahuan tiket anda';
        $email->AltBody = 'Tiket Anda';
        //This should be the same as the domain of your From address
        $email->DKIM_domain = 'rajendra-tours.com';
        //Path to your private key file
        $email->DKIM_private = 'dkim_private.pem';
        //Set this to your own selector
        $email->DKIM_selector = 'phpmailer';
        //If your private key has a passphrase, set it here
        $email->DKIM_passphrase = '';
        //The identity you're signing as - usually your From address
        $email->DKIM_identity = $email->From;
        
        if(!$email->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $email->ErrorInfo;
        } else {
           // echo 'Message has been sent.';
        }


    }
    public function upload_file($src,$name,$target){
         //pindahkan file
         $namafile = "NULL";
         $base = explode(",",$src);
         $file = str_replace("data:","",$base[0]);
         $file = str_replace("base64","",$file);
         switch($file){
             case "image/jpeg" : $type = ".jpeg"; break;
             case "image/png" : $type = ".png"; break;
             case "application/pdf" : $type = ".pdf"; break;
         }
         $srt_h = base64_decode($base[1]);
         $unik = date("YmdHis")."_";
         file_put_contents($target.$unik.$name.$type,$srt_h);
         $namafile = $unik.$name.$type;
         return $namafile;
    }
    public function add(){
        $this->load->rules("form");
        $this->load->model("aksi");
        $k = "'".$this->input->post("layanan")."',";
        $k .= "'".$this->input->post("nama")."',";
        $k .= "'".$this->input->post("instansi")."',";
        $k .= "'".$this->input->post("jalan")."',";
        $k .= "'".$this->input->post("kab")."',";
        $k .= "'".$this->input->post("telp")."',";
        $k .= "'".$this->input->post("hp")."',";
        $k .= "'".$this->input->post("email")."',";
        $k .= "'".$this->input->post("ket")."',";
        $srt = $this->upload_file($this->input->post("srt"),"file_surat","../view/upload/");
        $brks = $this->input->post("berkas");
        if($brks == ""){} else {
            $berkas = $this->upload_file($this->input->post("berkas"),"file_berkas","../view/upload/");
        }
        $ttd = $this->upload_file($this->input->post("ttd"),"file_ttd","../view/upload/");
        $k .= "'$srt','$berkas','$ttd'"; 
        $res = $this->db->add_form($k);
        echo $res;
    }
    public function imagery(){
        $this->load->rules("form"); 
        $this->load->model("aksi");
        $res = $this->db->get_image();
        echo $res;
    }
}

?>