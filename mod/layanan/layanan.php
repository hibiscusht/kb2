<?php 

class layanan extends mycelium_controller {
    public function index(){
        $det = $this->input->get("details");
        $dtl = explode("/",$det);
        echo $this->$dtl[0]($dtl); 
    }
    public function gets(){
        $this->load->rules("layanan");
        $this->load->model("aksi");
        $res = $this->db->get_layanan();
        echo $res;
    }
    public function gets2($dtl){
        $this->load->rules("layanan");
        $this->load->model("aksi");
        $res = $this->db->get_layanan2($dtl[1]);
        echo $res;
    }
    public function add($dtl){
        $this->load->rules("layanan");
        $this->load->model("aksi");
        $lyn = $this->input->post("lyn");
        $res = $this->db->add_layanan($lyn,$dtl[1]);
        echo $res;
    }
    public function update($dtl){
        $this->load->rules("layanan");
        $this->load->model("aksi");
        $lyn = $this->input->post("lyn");
        $id = $this->input->post("lyn_id");
        $res = $this->db->update_layanan($id,$lyn,$dtl[1]);
        echo $res;
    }
    public function delete($dtl){
        $this->load->rules("layanan");
        $this->load->model("aksi");
        $lyn = $dtl[2];
        $res = $this->db->del_layanan($lyn,$dtl[1]);
        echo $res;
    }
}

?>