<?php 

class user extends mycelium_controller {
    public function index(){
        $det = $this->input->get("details");
        $dtl = explode("/",$det);
        echo $this->$dtl[0]($dtl); 
    }
    public function edituser($dtl){
        $this->load->rules("user");
        $this->load->model("aksi");
        $token = $dtl[1];
        $ret = $this->db->get_user2($token);
        echo $ret;
    }
    public function getsall(){ 
        $this->load->rules("user");
        $this->load->model("aksi");
        $res = $this->db->get_user(); 
        echo $res;
    }
    public function delete($dtl){ 
        $this->load->rules("user");
        $this->load->model("aksi");
        $res = $this->db->hapus_user($dtl[1],$dtl[2]); 
        echo $res;
    }
    public function add($dtl){
        $this->load->rules("user");
        $this->load->model("aksi");
        $uname = $this->input->post("uname");
        $pwd = md5($this->input->post("pwd"));
        $name = $this->input->post("nama");
        $stt = $this->input->post("stt"); 
        if($_FILES["pic"]["name"] == ""){
            $rs = $this->db->tambah_user($uname,$pwd,$name,$stt,"NULL",$dtl[1]);
            echo "no file";
        } else {
            $dirs = "../view/img/";
            $pics = rand(1,1000000)."_".$_FILES["pic"]["name"];
            echo $dirs.$pics; 
            move_uploaded_file($_FILES["pic"]["tmp_name"],$dirs.$pics);
            $rs = $this->db->tambah_user($uname,$pwd,$name,$stt,$pics,$dtl[1]);
        }
    }
    public function update($dtl){
        $this->load->rules("user");
        $this->load->model("aksi");
        $uname = $this->input->post("uname");
        $pass =  $this->input->post("pwd");
        $pwd = ($pass == "")? "NULL" : md5($pass);
        $name = $this->input->post("nama");
        $stt = $this->input->post("stt"); 
        $id = $this->input->post("edit"); 
        if($_FILES["pic"]["name"] == ""){
            $rs = $this->db->update_user($uname,$pwd,$name,$stt,"NULL",$dtl[1],$id);
            echo "no file";
        } else {
            $rst = $this->db->get_user($id);
            $dirs = "../view/img/"; 
            $rsts = json_decode($rst,true);
            unlink($dirs.$rsts["rows"][0]["pic"]) or die();
            $pics = rand(1,1000000)."_".$_FILES["pic"]["name"];
            echo $dirs.$pics; 
            move_uploaded_file($_FILES["pic"]["tmp_name"],$dirs.$pics);
            $rs = $this->db->update_user($uname,$pwd,$name,$stt,$pics,$dtl[1],$id);  
        }
    }
}

?>