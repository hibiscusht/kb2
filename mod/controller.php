<?php

class mycelium_controller {
    public $input;
    public $db;
    public $load;
    public $rules;
    public function __construct(){
        $this->input = $this;
        $this->load = $this;
    }
    public function __call($name,$val = null){
        $lib = new $name($val[0]);
        return $lib;
    }
    public function rules($rule){
        $this->rules = $rule;
    }
    public function model($model){
        require $this->rules."/".$model.".php";
        $this->db = new $model();
    }
    public function library($name){
        require  "library/".$name."/".$name.".php";
    }
    public function post($post){
        $ptn = array("/;/","/'/");
        $repl = array("","");
        $p = preg_replace($ptn,$repl,$_POST[$post]);
        return $p;
    }
    public function get($get){
        $ptn = array("/;/","/'/");
        $repl = array("","");
        $g = preg_replace($ptn,$repl,$_GET[$get]);
        return $g;
    }
}

?>