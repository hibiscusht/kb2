-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 07. Nopember 2020 jam 13:03
-- Versi Server: 5.1.41
-- Versi PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `restform`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `konfirmasi`
--

DROP TABLE IF EXISTS `konfirmasi`;
CREATE TABLE IF NOT EXISTS `konfirmasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permohonan` int(11) DEFAULT NULL,
  `ket` text,
  `file` date DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `user` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data untuk tabel `konfirmasi`
--


-- --------------------------------------------------------

--
-- Struktur dari tabel `layanan`
--

DROP TABLE IF EXISTS `layanan`;
CREATE TABLE IF NOT EXISTS `layanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data untuk tabel `layanan`
--

INSERT INTO `layanan` (`id`, `nama`) VALUES
(1, 'Ahli Bahasa'),
(11, 'Bahasa Pemrograman Web'),
(3, 'BIPA'),
(5, 'Buku'),
(6, 'Penyuluh'),
(7, 'UKBI'),
(8, 'Jurnal'),
(9, 'Perpustakaan'),
(12, 'BAFO'),
(13, 'UFO'),
(14, 'FOFO');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE IF NOT EXISTS `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` text,
  `act` text,
  `wkt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data untuk tabel `log`
--

INSERT INTO `log` (`id`, `user`, `act`, `wkt`) VALUES
(1, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-14 10:52:06'),
(2, '3def184ad8f4755ff269862ea77393dd', 'logout', '2020-09-14 11:04:13'),
(3, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-14 11:55:39'),
(4, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-14 13:52:36'),
(5, '3def184ad8f4755ff269862ea77393dd', 'delete service', '2020-09-14 13:55:40'),
(6, '3def184ad8f4755ff269862ea77393dd', 'delete service', '2020-09-14 13:56:18'),
(7, '3def184ad8f4755ff269862ea77393dd', 'delete user', '2020-09-14 00:00:00'),
(8, '3def184ad8f4755ff269862ea77393dd', 'add user', '2020-09-14 14:14:10'),
(9, '3def184ad8f4755ff269862ea77393dd', 'update user', '2020-09-14 14:29:34'),
(10, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-15 15:26:20'),
(11, '3def184ad8f4755ff269862ea77393dd', 'logout', '2020-09-15 15:26:27'),
(12, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-16 14:33:27'),
(13, '3def184ad8f4755ff269862ea77393dd', 'logout', '2020-09-16 14:36:10'),
(14, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-16 14:41:47'),
(15, '3def184ad8f4755ff269862ea77393dd', 'logout', '2020-09-16 15:39:52'),
(16, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-17 09:59:27'),
(17, '3def184ad8f4755ff269862ea77393dd', 'add service', '2020-09-17 10:14:07'),
(18, '3def184ad8f4755ff269862ea77393dd', 'update service', '2020-09-17 10:14:24'),
(19, '3def184ad8f4755ff269862ea77393dd', 'delete service', '2020-09-17 10:14:38'),
(20, '3def184ad8f4755ff269862ea77393dd', 'add service', '2020-09-17 10:55:58'),
(21, '3def184ad8f4755ff269862ea77393dd', 'add service', '2020-09-17 10:59:18'),
(22, '3def184ad8f4755ff269862ea77393dd', 'add service', '2020-09-17 11:05:20'),
(23, '3def184ad8f4755ff269862ea77393dd', 'reject application', '2020-09-17 00:00:00'),
(24, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-18 14:18:47'),
(25, '3def184ad8f4755ff269862ea77393dd', 'update settings', '2020-09-18 16:11:57'),
(26, '3def184ad8f4755ff269862ea77393dd', 'confirm application', '2020-09-18 00:00:00'),
(27, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-19 10:59:40'),
(28, '3def184ad8f4755ff269862ea77393dd', 'login', '2020-09-23 14:51:00'),
(29, '3def184ad8f4755ff269862ea77393dd', 'logout', '2020-09-23 14:51:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `module`
--

DROP TABLE IF EXISTS `module`;
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) DEFAULT NULL,
  `name` text,
  `link` text,
  `status` int(11) DEFAULT NULL,
  `orders` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data untuk tabel `module`
--

INSERT INTO `module` (`id`, `parent`, `name`, `link`, `status`, `orders`) VALUES
(1, 0, 'Home', 'home', 1, 1),
(2, 0, 'Jenis Layanan', 'layanan-table', 1, 2),
(3, 0, 'Admin', 'user-table', NULL, 3),
(4, 0, 'Permohonan', 'permohonan-table', 1, 2),
(5, 0, 'Pengaturan', 'setting-table', 1, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `permohonan`
--

DROP TABLE IF EXISTS `permohonan`;
CREATE TABLE IF NOT EXISTS `permohonan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layanan` int(11) DEFAULT NULL,
  `nama` varchar(128) DEFAULT NULL,
  `instansi` text,
  `alamat_jln` text,
  `alamat_kota` text,
  `telp` text,
  `hp` text,
  `email` text,
  `ket` text,
  `f_srt` text,
  `f_brks` text,
  `ttd` text,
  `tgl` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `permohonan`
--

INSERT INTO `permohonan` (`id`, `layanan`, `nama`, `instansi`, `alamat_jln`, `alamat_kota`, `telp`, `hp`, `email`, `ket`, `f_srt`, `f_brks`, `ttd`, `tgl`, `status`) VALUES
(1, 13, 'lorem', '1', '', '', '', '1', '1', '1', '20200919093432_file_surat.jpeg', '', '20200919093432_file_ttd.png', '2020-09-19', NULL),
(2, 11, 'ff', 'ff', '', '', '', 'ff', 'ff', 'ff', '20200919094429_file_surat.jpeg', '', '20200919094429_file_ttd.png', '2020-09-19', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

DROP TABLE IF EXISTS `setting`;
CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text,
  `isi` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`id`, `nama`, `isi`) VALUES
(1, 'Syarat & Ketentuan', '<p><b>SYARAT DAN KETENTUAN</b></p><ol><li><b>Lorem Ipsum dolor</b></li><li><b>Dolor sit amet</b></li></ol>'),
(2, 'Hubungi Kami', '<p>SMS/WA 085659249959<br></p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text,
  `password` text,
  `reset` text,
  `nama` varchar(128) DEFAULT NULL,
  `token` text,
  `pic` text,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nama` (`nama`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `reset`, `nama`, `token`, `pic`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, 'Administrator', '3def184ad8f4755ff269862ea77393dd', NULL, 1),
(3, 'hero', 'f04af61b3f332afa0ceec786a42cd365', NULL, 'Super Hero', '9d5c5be4dfa3778f4c988d6deabd233c', '955597_avatar-05.jpg', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
