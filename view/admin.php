<?php
require "../mod/config/conf.php";

$det = explode("/",$_GET["details"]); 
switch($det[0]){ 

    case "home": ?> 


<!DOCTYPE html>
<html>


<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Aplikasi Manajemen Surat</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

 <!-- <link rel="shortcut icon" href="<?php echo $base_url; ?>view/assets/ugm.png">   --> 
  
<!-- jQuery 3 -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
 <!-- <script src="<?php echo $base_url; ?>view/assets/js/jquery.js"></script> -->
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/bower_components/bootstrap/dist/css/bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/bower_components/font-awesome/css/font-awesome.min.css"> 
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/bower_components/Ionicons/css/ionicons.min.css"> 
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/dist/css/AdminLTE.min.css"> 
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/dist/css/skins/_all-skins.min.css"> 
  <!--
    
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/bower_components/morris.js/morris.css"> 
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"> 
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
 
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/bower_components/bootstrap-daterangepicker/daterangepicker.css"> 
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/css/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo $base_url; ?>view/assets/css/dataTables.bootstrap.min.css">

  <script type="text/javascript" src="<?php echo $base_url; ?>view/assets/datatables/js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo $base_url; ?>view/assets/DataTables/media/js/jquery.dataTables.js"></script>
  <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>view/assets/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>view/assets/datatables/css/jquery.dataTables.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>view/assets/datatables/css/dataTables.bootstrap">

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  
<script src="<?php echo $base_url; ?>view/assets/bower_components/bootstrap/dist/js/jquery.autocomplete.js"></script>
<script src="<?php echo $base_url; ?>view/assets/bower_components/bootstrap/dist/js/jquery.autocomplete.min.js"></script>
   -->

 
</head>

<style>
.hidden {display: none}
        </style>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper bodies hidden">

<style>
.logos {max-width: 90px;overflow: hidden;text-indent: 38px;margin-left: -18%;}
.logos-img {max-width: 180px !important}
.logos-txt {position: relative;top: -52px;left: 30px;bottom: 0;font-size: 0.6em;width: 100px;}
@media (max-width: 991px){
.logos {max-width: 106px;overflow: hidden;text-indent: 42pt;margin-left: 15%;}
.logos-img {max-width: 180px !important}
.logos-txt {position: relative;top: -52px;left: 30px;bottom: 0;font-size: 0.6em;width: 100px;}
}
</style>

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><div class="logos">
<img src="" alt="@hibiscusht" class="imagery logos-img"/></div></span>
<span class="logos-txt"><b>Balai Bahasa Prov Bali</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><i class="fa fa-cogs"></i>&emsp;Administrator</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header" style="color: white;">
                <img src="<?php echo $base_url; ?>view/assets/userchat.png" style="color: white;" class="img-circle" alt="User Image">

                <p>
                  Administrator                  <small>NIP : 1703064</small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="?page=pro" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo $base_url; ?>admin/logout/<?php echo $det[1]; ?>" class="btn btn-default btn-flat">Logout</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    
<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" >
        <div class="pull-left image">
          <img src="<?php echo $base_url; ?>view/assets/userchat.png" style="background-color: black;" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Administrator</p>
          <a href="#"></i> NIP : 1703064</a>
        </div>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu modules" data-widget="tree">
        <li class="header">MENU UTAMA</li>
      </ul>
    </section>

    

    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper"  style="background-image: url(asset/img/bg2.jpg);">
    <!-- Content Header (Page header) -->

    
    <div class="global home">
     <!-- Main content -->
    <section class="content">
      <div class="callout callout-info">
          <h4>Selamat Datang Administrator !</h4>

          <p>Anda login sebagai 
          <strong>Super Admin</strong>. Anda memiliki akses penuh terhadap sistem.</p></p>
        </div>
        <div class="callout callout-success">
          
                <div class="col s12" id="header-instansi">
                    <div class="card blue-grey white-text">
                        <div class="card-content"><h5 class="ins">Balai Bahasa Provinsi Bali</h5><h5 class="ins"> Status : Aktif</h5><h5 class="ins"></h5><p class="almt"></p>
                        </div>
                    </div>
                </div>        </div>
      <!-- Small boxes (Stat box) -->
      <div class="row">
                <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
                <h3 class="prs">0</h3>
              <p>Permohonan Diproses</p>
            </div>
            <div class="icon">
              <i class="ion ion-email"></i>
            </div> 
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 class="sls">0</h3>
              <p>Permohonan Selesai</p>
            </div>
            <div class="icon">
              <i class="ion ion-email"></i>
            </div>
             
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 class="week">0</h3>
              <p>Permohonan Minggu Ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-document-text"></i>
            </div> 
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 class="tot">0</h3>
              <p>Total Permohonan</p>
            </div>
            <div class="icon">
              <i class="ion ion-bookmark"></i>
            </div>
             
          </div>
        </div>

<script>
async function notif(){
    $(".check").addClass("hidden"); 
    let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/stat");
    let koh = await ah.json();
    if(ah.ok){
        $(".tot").html(koh.total);
        $(".week").html(koh.minggu);
        $(".sls").html(koh.selesai);
        $(".prs").html(koh.proses);
    }
}
notif();
</script>
                   
                    <!-- ./col -->
      </div>
            <!-- /.row -->
      <!-- Main row -->
      
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>

    <!-- module layanan -->

    <div class="global hidden" id="layanan-table">
    <section class="content-header">
  <h1>
   Jenis Layanan    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Jenis Layanan</li>
  </ol>
</section>

<section class="content">
<style>
th { background: #c5c5c5; color: black; font-weight: bold; text-align: center}
span.select2 { width: 100% !important }
</style>

 <div class="callout callout-success">
 
 <div class="row">
 <div class="col-md-6">
 <a href="#" onclick="cycles('layanan-table','layanan-input')"><button class="btn btn-info">Tambah Data Jenis Layanan</button></a>
 </div>

 </div>
 </div>

<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <div class="input-group" style="padding-bottom: 1%; width: 30%; float: right">
          <input type="text" id="q-layanan-table" class="form-control" placeholder="Cari jenis layanan...">
          <span class="input-group-btn">
                <button type="button" id="layanan-table_q" class="btn btn-flat" onclick="cari(this.id)"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
            <div id="user_example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
               <table class="table table-bordered">
               <thead>
               <tr>
               <th>Aksi</th>
               <th>Jenis Layanan</th>
               </tr>
               </thead>
               <tbody id="lyn-dt">
               </tbody>
               </table>
            </div>
          <!-- /.box-body -->
       </div></div>

      </section>


</div>


<div class="global hidden" id="layanan-input">
    <section class="content-header">
  <h1>
  Data Jenis Layanan   <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data Jenis Layanan</li>
  </ol>
</section>

<section class="content">
<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          
 <form role="form">
          <div class="box-body">
            <div id="user_example_wrapper">
            <input type="hidden" id="lyn-edit" class="clear">
<div class="form-group">
<label for="lyn" class="control-label mb-1">Jenis Layanan</label>
<input id="lyn" type="text" class="clear form-control">
</div>
<div>
<button type="button" class="btn btn-sm btn-info"> 
<span id="lyn-simpan" onclick="lynSimpan()">Simpan</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('layanan-input','layanan-table')"> 
<span>Batal</span>
</button>
</div>
            </div>
          <!-- /.box-body -->
       </div></div>
       </form>
</section>

</div>

<!-- module layanan -->

<!-- module user -->

<div class="global hidden" id="user-table">
    <section class="content-header">
  <h1>
   Admin    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Admin</li>
  </ol>
</section>

<section class="content">
<style>
.tbl td { padding: 5px }
span.select2 { width: 100% !important }
</style>

 <div class="callout callout-success">
 
 <div class="row">
 <div class="col-md-6">
 <a href="#" onclick="cycles('user-table','user-input')"><button class="btn btn-info">Tambah Data Admin</button></a>
 </div>

 </div>
 </div>

<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div id="user_example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                hello
            </div>
          <!-- /.box-body -->
       </div></div>
    
      
      
      </section>

</div>

<div class="global hidden" id="user-input">
    <section class="content-header">
  <h1>
  Data Admin    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data Admin</li>
  </ol>
</section>

<section class="content">
<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          
 <form role="form">
          <div class="box-body">
            <div id="user_example_wrapper">
              
            <input type="hidden" id="user-edit" class="clear"/>
<div class="form-group">
<label for="user" class="control-label mb-1">Username</label>
<input id="user" type="text" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-pwd" class="control-label mb-1">Password <span class="cek_edit"></span></label>
<input id="user-pwd" type="password" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-sta" class="control-label mb-1">Status</label>
<select class="clear form-control" id="user-sta">
<option value="">--silakan pilih--</option>
<option value="0">Tidak Aktif</option>
<option value="1">Aktif</option>
</select>
</div>

<div class="form-group">
<label for="user-name" class="control-label mb-1">Nama Lengkap</label>
<input id="user-name" type="text" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-pic" class="control-label mb-1">Profile Pic <span class="cek_edit"></span></label>
<input id="user-pic" type="file" class="clear form-control" />
</div>

<div>
<button type="button" class="btn btn-sm btn-info">
<span id="user-simpan" onclick="userSimpan()">
<i class="fa fa-lock fa-lg"></i>&nbsp;Simpan</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('user-input','user-table')">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span>Batal</span>
</button>
</div>


            </div>
          <!-- /.box-body -->
       </div></div>
       </form>
</section>

</div>

<!-- module user -->

<!-- module permohonan -->

<div class="global hidden" id="permohonan-table">
    <section class="content-header">
  <h1>
   Formulir Permohonan   <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Formulir Permohonan</li>
  </ol>
</section>

<section class="content">
<style>
.tbl td { padding: 5px }
span.select2 { width: 100% !important }
</style>

 <div class="callout callout-success">
 
 <div class="row">
 <div class="col-md-6"><!--
 <a href="#" onclick="cycles('permohonan-table','permohonan-input')"><button class="btn btn-info">Tambah Data Permohonan</button></a> -->
 </div>

 </div>
 </div>

<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
          <div class="input-group" style="padding-bottom: 1%; width: 30%; float: right">
          <input type="text" id="q-permohonan-table" class="form-control" placeholder="Cari nama pemohon...">
          <span class="input-group-btn">
                <button type="button" id="permohonan-table_q" class="btn btn-flat" onclick="cari(this.id)"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
            <div id="user_example_wrapper" >
            <table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>Aksi</th>
<th>Jenis Layanan</th>
<th>Nama Pemohon</th>
</tr>
</thead>
<tbody id="mhn-dt"> </tbody>
</table>
            </div>
          <!-- /.box-body -->
       </div></div>
    
      
      
      </section>

</div>

<div class="global hidden" id="permohonan-input">
    <section class="content-header">
  <h1>
  Data Permohonan    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data Permohonan</li>
  </ol>
</section>

<section class="content">
<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          
 <form role="form">
          <div class="box-body">
            <div id="user_example_wrapper">
     
            <input type="hidden" id="konfirm-edit" class="clear">
<input type="hidden" id="konfirm-layanan" class="clear">
<div class="form-group">
<label for="lyn" class="control-label mb-1">Jenis Layanan</label>
<div id="konfirm-jns"></div>
</div> 
<div class="form-group">
<label for="lyn" class="control-label mb-1">Nama Pemohon</label>
<div id="konfirm-pemohon"></div>
</div>
<div class="form-group">
<label for="lyn" class="control-label mb-1">File Pemohon</label>
<div id="konfirm-files"></div>
</div>
<div class="form-group">
<label for="lyn" class="control-label mb-1">Pesan</label>
<textarea id="konfirm" class="form-control"></textarea>
</div> 
<div class="form-group">
<label for="lyn" class="control-label mb-1">Lampiran</label> 
<input type="file" />
</div> 
<div>
<button type="button" class="btn btn-sm btn-info"> 
<span id="konfirm-simpan" onclick="konfirmSimpan()">Kirim</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('permohonan-input','permohonan-table')"> 
<span>Batal</span>
</button>
</div>

            </div>
          <!-- /.box-body -->
       </div></div>
       </form>
</section>

</div>

<!-- module permohonan -->

<!--  module pengaturan -->

<div class="global hidden">
    <section class="content-header">
  <h1>
   ...    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">...</li>
  </ol>
</section>

<section class="content">
<style>
.tbl td { padding: 5px }
span.select2 { width: 100% !important }
</style>

 <div class="callout callout-success">
 
 <div class="row">
 <div class="col-md-6">
 <a href="#" onclick="cycles('...-table','...r-input')"><button class="btn btn-info">Tambah Data ...</button></a>
 </div>

 </div>
 </div>

<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div id="user_example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                hello
            </div>
          <!-- /.box-body -->
       </div></div>
    
      
      
      </section>

</div>

<div class="global hidden" id="setting-table">
    <section class="content-header">
  <h1>
  Data Pengaturan   <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data Pengaturan</li>
  </ol>
</section>

<section class="content">
<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          
 <form role="form">
          <div class="box-body">
            <div id="user_example_wrapper">
       <span id="awaiting"><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</span>
<div class="form-group">
<label for="setting-cond" class="control-label mb-1">Syarat & Ketentuan</label> 
<textarea id="setting-cond" class="summernote"></textarea>
</div> 
<div class="form-group">
<label for="setting-cont" class="control-label mb-1">Hubungi Kami</label> 
<textarea id="setting-cont" class="summernote"></textarea>
</div> 
<div>
<button type="button" class="btn btn-sm btn-info"> 
<span id="setting-simpan" onclick="settingSimpan()">Simpan</span>
</button> 
</div>

             
          <!-- /.box-body -->
       </div></div>
       </form>
       </div></div></div>
</section>

</div>

<!-- module pengaturan -->


<!-- new module -->

<div class="global hidden" id="...-table">
    <section class="content-header">
  <h1>
   ...    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">...</li>
  </ol>
</section>

<section class="content">
<style>
.tbl td { padding: 5px }
span.select2 { width: 100% !important }
</style>

 <div class="callout callout-success">
 
 <div class="row">
 <div class="col-md-6">
 <a href="#" onclick="cycles('...-table','...r-input')"><button class="btn btn-info">Tambah Data ...</button></a>
 </div>

 </div>
 </div>

<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div id="user_example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                hello
            </div>
          <!-- /.box-body -->
       </div></div>
    
      
      
      </section>

</div>

<div class="global hidden" id="...-input">
    <section class="content-header">
  <h1>
  Data ...    <small></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Data ..</li>
  </ol>
</section>

<section class="content">
<div class="row">
      <div class="col-xs-12"><div class="box">
          <div class="box-header">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          
 <form role="form">
          <div class="box-body">
            <div id="user_example_wrapper">
     


            </div>
          <!-- /.box-body -->
       </div></div>
       </form>
</section>

</div>

<!-- new module -->

  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>@2019 SIP-KAN Sekolah Vokasi UGM</strong>
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<script>

async function set_desc(){
   let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/imagery");
    let btp = await ah.json();
    if(ah.ok){ 
        $(".imagery").attr("src","<?php echo $logo_url; ?>view/images/icon/" + btp.rows[0].img);
    } 
}
set_desc();

function cycles(a,b){
    $("#" + b).removeClass("hidden");
    $("#" + a).addClass("hidden");
    $(".clear").val("");  
    
}

async function unhides(id){
    $(".global").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
         
          
          $("#" + id).removeClass("hidden");
 
          populates(id);

        }

        async function get_data(id,chk){
          let cache = JSON.parse(localStorage.getItem(id));
    switch(id){
        case "layanan-table": 
        $(".removed").remove();
        let k = "";
        for(let i in cache.rows){
            k += "<tr class='removed'><td><button class='btn btn-sm btn-danger' id='del-" + cache.rows[i].id + "' onclick='del(this.id)' data-type='layanan-table'>Hapus</button> <button class='btn btn-sm btn-info' id='edit-" + cache.rows[i].id + "' onclick='edit(this.id)' data-type='layanan'>Edit</button></td><td>" + cache.rows[i].nama + "</td></tr>";
        }
        $("#" + chk + "-dt").append(k);
         break;
         case "permohonan-table":
         $(".removed").remove();
         let jk = "";
         for(let i in cache.rows){
             let aksi = "";
             if(cache.rows[i].konfirm == null){
                if(cache.rows[i].status == null){
                    aksi = "<button id='" + cache.rows[i].id + "' class='btn btn-sm btn-info' onclick='formTerima(this.id)' data-type='entry'>Terima</button>&nbsp;<button id='tlk-" + cache.rows[i].id + "' class='btn btn-sm btn-danger' onclick='formTolak(this.id)' data-type='entry'>Tolak</button>";
                } else {
                    aksi = "<span class='badge badge-danger'>ditolak</span>";
                }
             } else {
                aksi = "<button id='" + cache.rows[i].id + "' class='btn btn-sm btn-info' onclick='formTerima(this.id)' >Kirim Ulang Konfirmasi</button>";
             } 
            jk += "<tr class='removed'><td>" + aksi + "</td><td>" + cache.rows[i].lyn + "</td><td>" + cache.rows[i].nama + "</td></tr>";
         }
         $("#" + chk + "-dt").append(jk);
         break;
         case "user-table":
         $(".removed").remove();
         let jki = "";
         for(let i in cache.rows){

             let status = (cache.rows[i].status == 1)? "Aktif" : "Tidak Aktif";
            
            jki += "<tr class='removed'><td><button class='btn btn-sm btn-danger' id='del-" + cache.rows[i].id + "' onclick='del(this.id)' data-type='user-table'>Hapus</button> <button class='btn btn-sm btn-info' id='edit-" + cache.rows[i].id + "' onclick='edit(this.id)' data-type='user'>Edit</button></td></td><td>" + cache.rows[i].nama + "</td><td>" + status + "</td></tr>";
         }
         $("#" + chk + "-dt").append(jki);
         break;
    }
}

async function populates(id){
  if(id == "setting-table"){
    

let ah = await fetch("<?php echo $base_url; ?>setting.json/setting-bin/all");
let btp = await ah.json();

if(ah.ok){
  $("#awaiting").hide();
  $("#setting-cond").summernote("reset");
  $("#setting-cond").summernote("pasteHTML",btp.rows[0].isi);
  $("#setting-cont").summernote("reset");
  $("#setting-cont").summernote("pasteHTML",btp.rows[1].isi);
}

  } else {
    populate(id);
    $("#awaiting").show();
  }
}

        async function populate(id){
    let chk = "";
    let url = "<?php echo $base_url; ?>";
    switch(id){
        case "layanan-table": chk = "lyn"; url += "layanan.json/layanan-bin/gets"; break;
        case "permohonan-table": chk = "mhn"; url += "form.json/form-bin/getsall"; break;
        case "user-table": chk = "u"; url += "user.json/user-bin/getsall"; break;
    }
    $("#" + chk + "-dt").append("<tr class='removed'><td><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</td></tr>");
    let cache = localStorage.getItem(id);
    if(typeof cache === "object"){
         //update cache
    let c = await fetch(url);
    let ca = await c.text();
    if(c.ok){
        localStorage.removeItem(id);
        localStorage.setItem(id,ca);
        get_data(id,chk);
    }
    
    //console.log("abc");
    } else {
        let wkt_update = <?php 
    
    $d = date("G");
    if($d % 1 == 0){
        echo "true";
    } else {
        echo "false";
    }
    
    ?>;

    if(wkt_update){
         //update cache
    let c = await fetch(url);
    let ca = await c.text();
    if(c.ok){
        localStorage.removeItem(id);
        localStorage.setItem(id,ca);
       get_data(id,chk);
    }
    } else {
       get_data(id,chk);
    }
   // console.log("abcd");
    }
}

async function edit(id){
    let ids = id.split("-");
    let types = $("#" + id).data("type"); 
        $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...");
        if(types == "layanan"){
            let c = await fetch("<?php echo $base_url; ?>layanan.json/layanan-bin/gets2/" + ids[1]);
            let cu = await c.json();
        if(c.ok){ 
            cycles(types + "-table",types + "-input");
            $("#lyn-edit").val(ids[1]);
            $("#lyn").val(cu.rows[0].nama);
        }
        } else if(types == "user"){
            let c = await fetch("<?php echo $base_url; ?>user.json/user-bin/edituser/" + ids[1]);
            let cu = await c.json();
        if(c.ok){ 
            cycles(types + "-table",types + "-input");
            $(".cek_edit").html("(kosongkan jika tidak diubah)");
            $("#user-edit").val(ids[1]);
            $("#user").val(cu.rows[0].username);
                $("#user-sta").val(cu.rows[0].status);
                $("#user-name").val(cu.rows[0].nama);
        }

        }
        $("#" + id).html("Edit");
}

function cari(id){
  let c = id.split("_");
  let cache = JSON.parse(localStorage.getItem(c[0]));
  let q = $("#q-" + c[0]).val();
  $(".removed").remove();
  switch(c[0]){
    case "layanan-table":
    $("#lyn-dt").append("<tr class='removed'><td><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</td></tr>");
    let lyn = "";
  for(let i in cache.rows){
    let re = RegExp(q,"gi");
    if( re.test(cache.rows[i].nama) ){
      lyn += "<tr class='removed'><td><button class='btn btn-sm btn-danger' id='del-" + cache.rows[i].id + "' onclick='del(this.id)' data-type='layanan-table'>Hapus</button> <button class='btn btn-sm btn-info' id='edit-" + cache.rows[i].id + "' onclick='edit(this.id)' data-type='layanan'>Edit</button></td><td>" + cache.rows[i].nama + "</td></tr>";
    }  
  }
  $(".removed").remove();
  $("#lyn-dt").append(lyn);
    break;
    case "permohonan-table":
    $("#mhn-dt").append("<tr class='removed'><td><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</td></tr>");
    let mhn = "";
  for(let i in cache.rows){
    let re = RegExp(q,"gi");
    if( re.test(cache.rows[i].nama) ){
      let aksi = "";
             if(cache.rows[i].konfirm == null){
                if(cache.rows[i].status == null){
                    aksi = "<button id='" + cache.rows[i].id + "' class='btn btn-sm btn-info' onclick='formTerima(this.id)' data-type='entry'>Terima</button>&nbsp;<button id='tlk-" + cache.rows[i].id + "' class='btn btn-sm btn-danger' onclick='formTolak(this.id)' data-type='entry'>Tolak</button>";
                } else {
                    aksi = "<span class='badge badge-danger'>ditolak</span>";
                }
             } else {
                aksi = "<button id='" + cache.rows[i].id + "' class='btn btn-sm btn-info' onclick='formTerima(this.id)' >Kirim Ulang Konfirmasi</button>";
             } 
            mhn += "<tr class='removed'><td>" + aksi + "</td><td>" + cache.rows[i].lyn + "</td><td>" + cache.rows[i].nama + "</td></tr>";

    }  
    
  }
  $(".removed").remove();
  $("#mhn-dt").append(mhn);
    break;
  }
  
}

async function del(id){
    let ids = id.split("-");
    let types = $("#" + id).data("type");
    let ask = confirm("apakah yakin menghapus?");
    if(ask){
        $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menghapus data...");
        if(types == "layanan-table"){
            let c = await fetch("<?php echo $base_url; ?>layanan.json/layanan-bin/delete/<?php echo $det[1]; ?>/" + ids[1]);
        if(c.ok){
            localStorage.removeItem(types);
            populate(types);
        }
        } else if(types == "user-table"){
            let c = await fetch("<?php echo $base_url; ?>user.json/user-bin/delete/<?php echo $det[1]; ?>/" + ids[1]);
        if(c.ok){
            localStorage.removeItem(types);
            populate(types);
        }
        }
        
    }
}

/* fungsi spesifik */

async function settingSimpan(){
  $("#setting-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menyimpan data...");
  let cond = $("#setting-cond").val();
  let cont = $("#setting-cont").val();
  let fd = new FormData();
  fd.append("cond",cond);
  fd.append("cont",cont);
  let ah = await fetch("<?php echo $base_url; ?>setting.json/setting-bin/simpan/<?php echo $det[1]; ?>",{
    method: "POST",
    body: fd
  });
  if(ah.ok){
    $("#setting-simpan").html("Simpan");
  }
}

async function formTolak(id){
    let ids = id.split("-");
    let ask = confirm("menolak permohonan akan menyebabkan permohonan yang ditolak tidak dapat diakses. lanjutkan?");
    if(ask){
        $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memproses data...");
        let k = await fetch("<?php echo $base_url; ?>konfirmasi.json/konfirmasi-bin/reject/<?php echo $det[1]; ?>/" + ids[1]);
        if(k.ok){ 
            localStorage.removeItem("permohonan-table");
            populate("permohonan-table");
        }
    }
}

async function formTerima(id){
    $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...");
    cycles("permohonan-table","permohonan-input");
    $("#" + id).html("Terima");
    $("#konfirm-layanan").val(id);
    let j = await fetch("<?php echo $base_url; ?>form.json/form-bin/edits/" + id);
    let x = await j.json();
    if(j.ok){
        $("#konfirm-jns").html(x.rows[0].lyn);
        $("#konfirm-pemohon").html(x.rows[0].nama);
        let url = "<?php echo $base_url; ?>view/upload/";
        let brks = (x.rows[0].f_brks == "")? "" : "<a href='" + url + x.rows[0].f_brks + "' target='_BLANK'><img src='" + url + x.rows[0].f_brks + "' style='width: 15%; cursor:pointer'/></a>&nbsp;&nbsp;";
        $("#konfirm-files").html("<a href='" + url + x.rows[0].f_srt + "'  target='_BLANK'><img src='" + url + x.rows[0].f_srt + "' style='width: 15%; cursor:pointer'/></a>&nbsp;&nbsp;<a href='" + url + x.rows[0].ttd + "'  target='_BLANK'><img src='" + url + x.rows[0].ttd + "' style='width: 15%; cursor:pointer'/></a>")
        let konfirm_id = (x.rows[0].konfirm == null)? 0 : x.rows[0].konfirm; 
        let ah = await fetch("<?php echo $base_url; ?>konfirmasi.json/konfirmasi-bin/gets/" + konfirm_id);
        let btp = await ah.json(); 
        let ret = "Terima";
        if(konfirm_id == 0){
            //kosong
        } else {
            if(ah.ok){
                $("#konfirm").val(btp.rows[0].ket);
                $("#konfirm-edit").val(btp.rows[0].id);
                ret = "Kirim Ulang Konfirmasi";
            }
           
        } 
        $("#" + id).html(ret);
    }
    
    
}

async function konfirmSimpan(){
    $("#konfirm-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> mengirim...");
    let konfirm = $("#konfirm-edit").val();
    let lyn = $("#konfirm-layanan").val();
    let ket = $("#konfirm").val();
    let fd = new FormData();
    if(konfirm == ""){
        //insert
        fd.append("lyn",lyn);
        fd.append("ket",ket);
        let j = await fetch("<?php echo $base_url; ?>konfirmasi.json/konfirmasi-bin/add/<?php echo $det[1]; ?>",{
            method: "POST",
            body: fd
        });
        if(j.ok){
            $("#konfirm-simpan").html("Kirim");
            cycles("permohonan-input","permohonan-table");
        }
    } else {
        //update 
        fd.append("ket",ket);
        fd.append("konfirm",konfirm);
        let j = await fetch("<?php echo $base_url; ?>konfirmasi.json/konfirmasi-bin/update/<?php echo $det[1]; ?>",{
            method: "POST",
            body: fd
        });
        if(j.ok){
            $("#konfirm-simpan").html("Kirim");
            cycles("permohonan-input","permohonan-table");
        }
    }
    localStorage.removeItem("permohonan-table");
    populate("permohonan-table");
}

async function lynSimpan(){
    $("#lyn-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menyimpan data...");
    let lyn = $("#lyn").val();
    let edt = $("#lyn-edit").val();
    let fd = new FormData();
    if(edt == ""){
        //insert
        fd.append("lyn",lyn);
        let c = await fetch("<?php echo $base_url; ?>layanan.json/layanan-bin/add/<?php echo $det[1]; ?>",{
            method: "POST",
            body: fd
        });
        if(c.ok){
            localStorage.removeItem("layanan-table");
            populate("layanan-table");
            cycles("layanan-input","layanan-table");
            $("#lyn-simpan").html("Simpan");
            fetch("<?php echo $base_url; ?>cache.json/cache-bin/cari");
        }
    } else {
        //update
        fd.append("lyn",lyn);
        fd.append("lyn_id",edt);
        let c = await fetch("<?php echo $base_url; ?>layanan.json/layanan-bin/update/<?php echo $det[1]; ?>",{
            method: "POST",
            body: fd
        });
        if(c.ok){
            localStorage.removeItem("layanan-table");
            populate("layanan-table");
            cycles("layanan-input","layanan-table");
            $("#lyn-simpan").html("Simpan");
        }
    }
}

/* fungsi spesifik */

    $(document).ready(function(){
        async function get_module(){
            let a = await fetch("<?php echo $base_url; ?>module.json/module-bin/gets");
            let b = await a.json();
            if(a.ok){
                let mdl = "";
                let fa = ["fa-home","fa-car","fa-user","fa-gear"];
                for(let i in b.rows){
                    if(b.rows[i].name == "Home"){
                        mdl += "<li><a href=''><i class='fa " + fa[i] + "'></i>" + b.rows[i].name + "</a></li>";
                    } else {
                        let lnk = '"' + b.rows[i].link + '"';
                        mdl += "<li onclick='unhides(" + lnk + ")'><a href='#'><i class='fa " + fa[i] + "'></i>" + b.rows[i].name + "</a></li>";
                    }
                    
                }
                $(".modules").append(mdl);
                $(".bodies").removeClass("hidden");
            }
        }
        get_module();
    });
   
    </script>

<!-- include summernote css/js -->
<link href="<?php echo $base_url; ?>view/summernote/summernote.min.css" rel="stylesheet">
<script src="<?php echo $base_url; ?>view/summernote/summernote.min.js"></script>
<script>
$(document).ready(function() {
  $('.summernote').summernote(); 
});
</script>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo $base_url; ?>view/assets/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo $base_url; ?>view/assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo $base_url; ?>view/assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo $base_url; ?>view/assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo $base_url; ?>view/assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo $base_url; ?>view/assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo $base_url; ?>view/assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo $base_url; ?>view/assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo $base_url; ?>view/assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo $base_url; ?>view/assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo $base_url; ?>view/assets/dist/js/demo.js"></script>
<script src="<?php echo $base_url; ?>view/assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo $base_url; ?>view/assets/js/dataTables.bootstrap.min.js"></script>

<!--
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="http://malsup.github.com/jquery.media.js"></script> -->
<script type="text/javascript">
</script>

</body>
</html>

<?php  
 break;
 case "login": 
?>


<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com/polygon/cooladmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 03:41:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="au theme template">
<meta name="author" content="Hau Nguyen">
<meta name="keywords" content="au theme template">

<title>Zoho Forms</title>

<link href="<?php echo $base_url; ?>view/css/font-face.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/wow/animate.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/slick/slick.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/select2/select2.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/css/theme.css" rel="stylesheet" media="all">

<script src="<?php echo $base_url; ?>view/vendor/jquery-3.2.1.min.js" type="text/javascript"></script>

</head>
<body class="animsition">
<style>
    .header {
        position: relative;
    z-index: 1;
    background-color: #006699;
    width: 100%;
    height: 50px;
    }
    .classy-nav-container {
        position: relative;
  z-index: 100;
  background-color: #ffffff;
    }
    .classy-navbar {
        width: 100%;
  height: 70px;
  padding: 0.5em 2em;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  -ms-grid-row-align: center;
  align-items: center;
    }
    .single-service-area {
        position: relative;
  z-index: 1;
  padding: 40px 20px;
  border: 1px solid #e5e5e5;
  text-align: center;
  -webkit-transition-duration: 500ms;
  -o-transition-duration: 500ms;
  transition-duration: 500ms;
    }
    .single-service-area:hover {
        border-color: transparent;
    -webkit-box-shadow: 0 2px 40px 8px rgba(15, 15, 15, 0.15);
    box-shadow: 0 2px 40px 8px rgba(15, 15, 15, 0.15);
    }
    .login-form {
        width: 50%;
        margin: 0 25% 0 25%;
    }
    @media (max-width: 991px){
        .header {
            height: 30px;
        }
        .single-service-area {
            padding: 30px 15px;
        }
        .login-form {
        width: 90%;
        margin: 5%;
    }
    }
    </style>

<div class="page-wrapper">
<div class="page-content--bge5" style="height: 90vh !important">
<div class="header"></div>
<div class="classy-nav-container">
<div class="container">
<div class="classy-navbar">
<div style="max-width: 58px; overflow: hidden; text-indent: -5px">
<img style="max-width: 350px !important; height: 61px;" src="" alt="@hibiscusht" class="imagery"></div> SISTEM INFORMASI PERSURATAN <br/>
Balai Bahasa Provinsi Bali
</div>
</div>
</div>
<div class="container" style="padding-top: 20px">
<div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="text-center">
                        <h2><b>Login </b></h2>
                        <p><b>Masukkan Username dan Password dengan benar</b></p>
                    </div>
                </div>
            </div> 
<div class="single-service-area">
<form action="#" method="post" class="login-form">
<div class="form-group">
<label> </label>
<input class="au-input au-input--full" type="text" id="uname" placeholder="username">
</div>
<div class="form-group">
<label> </label>
<input class="au-input au-input--full" type="password" id="pwd" placeholder="password">
</div>

<button class="au-btn au-btn--block au-btn--green m-b-20" type="button" onclick="kirim()" id="send">Login</button>
</form>
</div>
</div>
</div>
</div> 
<footer class="footer" style="background-color: #2F4F4F; color: white; padding-top: 10px;">
        <div class="container">
            <div class="row justify-content-between">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-footer-widget">
                        <!-- Footer Logo -->
                        <p class="mb-30" style="background-color: #2F4F4F; color: white;"></p>

                        <!-- Copywrite Text -->
                        
                    </div>
                </div>

                <!-- Single Footer Widget -->

                <!-- Single Footer Widget -->
                <div class="col-12 col-md-4 col-xl-3">
                    <div class="single-footer-widget">
                        <!-- Contact Address -->
                        <div class="contact-address">
                            <p><b style=" color: white;">BALAI BAHASA BALI</b></p>
                            <small>"...................................."</small>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>
<script>
async function set_desc(){
   let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/imagery");
    let btp = await ah.json();
    if(ah.ok){ 
        $(".imagery").attr("src","<?php echo $logo_url; ?>view/images/icon/" + btp.rows[0].img);
    } 
}
set_desc();
    async function kirim(){
        $("#send").attr("disabled",true);
        $("#send").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memvalidasi...");
        let uname = $("#uname").val();
        let pwd = $("#pwd").val();
        let hash = btoa(uname + ":" + pwd);
        let k = await fetch("<?php echo $base_url; ?>login.json/login-bin/validate/" + hash);
        let m = await k.json();
        if(m.status != "invalid"){
            $("#send").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> mengalihkan...");
            location.href="<?php echo $base_url; ?>admin/home/" + m.status + "";
        } else {
            $("#send").attr("disabled",false);
            $("#send").html("Login");
            alert("username/password salah");
        }
    }
    </script>
    
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/popper.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/bootstrap.min.js" type="text/javascript"></script>

<script src="<?php echo $base_url; ?>view/vendor/slick/slick.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/wow/wow.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/animsition/animsition.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/counter-up/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/counter-up/jquery.counterup.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/circle-progress/circle-progress.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/perfect-scrollbar/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/chartjs/Chart.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/select2/select2.min.js" type="text/javascript">
    </script>

<script src="<?php echo $base_url; ?>view/js/main.js" type="text/javascript"></script>
</body>

<!-- Mirrored from colorlib.com/polygon/cooladmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 03:41:59 GMT -->
</html>
  <?php
    break;
    case "logout": 
    ?>
    <script>
    async function send(){
        let hash = "<?php echo $det[1]; ?>";
        let k = await fetch("<?php echo $base_url; ?>login.json/login-bin/invalidate/" + hash);
        let j = await k.json();
        if(j.status == "login again"){
            alert("terimakasih telah menggunakan aplikasi kamus");
            location.href = "<?php echo $base_url; ?>admin/login";
        }
    }
    send();
    </script>
    <?php
    break;

}

    ?>