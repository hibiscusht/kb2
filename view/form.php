<?php
require "../mod/config/conf.php";
?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com/polygon/cooladmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 03:41:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="au theme template">
<meta name="author" content="Hau Nguyen">
<meta name="keywords" content="au theme template">

<title>Zoho Forms</title>

<link href="<?php echo $base_url; ?>view/css/font-face.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/wow/animate.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/slick/slick.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/select2/select2.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/css/theme.css" rel="stylesheet" media="all">

<script src="<?php echo $base_url; ?>view/vendor/jquery-3.2.1.min.js" type="text/javascript"></script>

<style>
.overview-item--cwt {
        background-image: -webkit-linear-gradient(90deg, #4272d7 0%, #4272d7 100%);
    }
.hidden {display: none}  
.fluids { margin: 1% 25% 0 25%}  

#bcPaint-header {display: none !important} 
canvas {border: none !important}
#bcPaint-canvas-container { height: 160px !important; width: 302px !important; }
.canvas-sign {height: 200px; border: solid #c3c3c3 2px; margin-bottom: 15%}
.canvas-sign-container {width: 40% !important; top: 1390px; left: 35%; position: absolute}
#bcPaint-export {display: none}
#bcPaint-reset {padding: 0 !important; height: 20px !important; font-size: 10pt !important; width: 30% !important; background: rgba(0,0,0,0) !important; color: #4e6871 !important; cursor: pointer; text-decoration: underline; margin-left: 100px !important; text-transform: capitalize !important}
#bcPaint-bottom {text-align: left !important; padding: 5% 0 0 0 !important;}
@media (max-width: 991px){
    .fluids {margin: 2%}
#bcPaint-canvas-container { height: 160px !important; width: 302px !important; } 
.canvas-sign {height: 200px; border: solid #c3c3c3 2px;  margin-bottom: 15%}
.canvas-sign-container {width: 40% !important; top: 1480px; left: 20%;  position: absolute}
#bcPaint-bottom {padding: 20% 0 0 0 !important;}
#bcPaint-reset {padding: 0 !important; height: 20px !important; font-size: 10pt !important; width: 100% !important; margin-left: 10px !important}
#bcPaint-bottom {background: rgba(0,0,0,0) !important}
}
</style>

</head>
<body class="animsition">


<div class="section__content section__content--p30" style="width: 100% ; padding: 0 !important">
<div class="page-wrapper">

<header class="header-desktop3 d-none d-lg-block">
<div class="section__content section__content--p35">
<div class="header4-wrap">
<div class="header__logo" style="width: 15%">
<a href="#" onclick="javascript: location.reload()">
<img src="" class="imagery" alt="@HibiscusHT" />
</a>
</div>
<div class="header__navbar">
<ul class="list-unstyled">
<li><a href="#" onclick="javascript: location.reload()"><i class='fa fa-home'></i> Depan</a></li>
<li><a href="#" onclick="gotos()"><i class='fa fa-plus-circle'></i> Permohonan Formulir</a></li>
<li><a href="#" onclick="about()"><i class='fa fa-lock'></i> Syarat & Ketentuan</a></li>
<li><a href="#" onclick="contact()"><i class='fa fa-paper-plane'></i> Hubungi Kami </a></li>
<li><a href="#" onclick="jump_cari()"><i class='fa fa-search'></i> Pencarian Proses Formulir</a></li>
</ul>
</div>
<!-- di sini -->
</div>
</div>
</header>


<header class="header-mobile header-mobile-2 d-block d-lg-none">
<div class="header-mobile__bar">
<div class="container-fluid">
<div class="header-mobile-inner">
<a class="logo" href="#"  onclick="javascript: location.reload()">
<img style="width: 50%" src="" class="imagery" alt="@HibiscusHT"  />
</a>
<button class="hamburger hamburger--slider" type="button">
<span class="hamburger-box">
<span class="hamburger-inner"></span>
</span>
</button>
</div>
</div>
</div>
<nav class="navbar-mobile">
<div class="container-fluid">
<ul class="navbar-mobile__list list-unstyled">
<li><a href="#" onclick="javascript: location.reload()"><i class='fa fa-home'></i> Depan</a></li>
<li><a href="#" onclick="gotos()"><i class='fa fa-plus-circle'></i> Permohonan Formulir</a></li>
<li><a href="#" onclick="about()"><i class='fa fa-lock'></i> Syarat & Ketentuan</a></li>
<li><a href="#" onclick="contact()"><i class='fa fa-paper-plane'></i> Hubungi Kami </a></li>
<li><a href="#" onclick="jump_cari()"><i class='fa fa-search'></i> Pencarian Proses Formulir</a></li>
</ul>
</div>
</nav>
</header>

<section class="au-breadcrumb2">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="au-breadcrumb-content">

<div class="au-breadcrumb-left">
<ul class="list-unstyled list-inline au-breadcrumb__list">
<li class="list-inline-item active">Anda Di Sini
</li>
<li class="list-inline-item seprate">
<span><i class="fa fa-arrow-circle-right"></i></span>
</li>
<li class="list-inline-item position">Depan</li>
</ul>
</div>

<form class="form-header" action="#" method="POST">
<input class="au-input au-input--xl" type="text" id="q" name="search" placeholder="Cari permohonan" />
<button class="au-btn--submit" type="button" onclick="carkat()">
<i class="zmdi zmdi-search"></i>
</button>
</form>

</div>
</div>
</div>
</div>

</section>

<!-- header stop --> 

<div class="container-fluid front hidden">
<div class="row">
<div class="col-lg-12">

<section class="welcome p-t-10">
<div class="container">
<div class="row">
<div class="col-md-12">
<h1 class="title-4">Selamat Datang
<span>Di Aplikasi Permohonan Formulir Balai Bahasa Bali</span>
</h1>
<hr class="line-seprate">
</div>
</div>
</div>
</section>

<section class="statistic statistic2">
<div class="container">
<div class="row">
<div class="col-md-6 col-lg-3">
<div class="statistic__item statistic__item--green">
<h2 class="number prs">0</h2>
<span class="desc">Sedang Proses</span>
<div class="icon">
<i class="zmdi zmdi-refresh-sync-off"></i>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3">
<div class="statistic__item statistic__item--orange">
<h2 class="number sls">0</h2>
<span class="desc">Formulir Selesai</span>
<div class="icon">
<i class="zmdi zmdi-check"></i>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3">
<div class="statistic__item statistic__item--blue">
<h2 class="number week">0</h2>
<span class="desc">Formulir Minggu ini</span>
<div class="icon">
<i class="zmdi zmdi-calendar-note"></i>
</div>
</div>
</div>
<div class="col-md-6 col-lg-3">
<div class="statistic__item statistic__item--red">
<h2 class="number tot">0</h2>
<span class="desc">Total Formulir</span>
<div class="icon">
<i class="zmdi zmdi-globe-alt"></i>
</div>
</div>
</div>
</div>
</div>
</section>

</div>
</div>
</div>

<div class="container-fluid hidden cont">
<div class="row ">
<div class="col-lg-12">
<div class="card bg-info">
<div class="card-body" style="color: white; font-weight: bold">
<span class="awaiting"><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</span>
<div id="data-cont"></div>
</div>
</div>
</div>
</div>
</div>

<div class="container-fluid hidden cond">
<div class="row ">
<div class="col-lg-12">
<div class="card bg-info">
<div class="card-body" style="color: white; font-weight: bold">
<span class="awaiting"><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</span>
<div id="data-cond"></div>
</div>
</div>
</div>
</div>
</div>

<div class="container-fluid check">
<div class="row fluids">
<div class="col-lg-12">
<div class="card bg-danger">
<div class="card-body" style="color: white; font-weight: bold">
<i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...
</div>
</div>
</div>
</div>
</div>

<div class="container-fluid save hidden">
<div class="row fluids">
<div class="col-lg-12">
<div class="card bg-success">
<div class="card-body" style="color: white; font-weight: bold">
<i class='fa fa-check-square' style="font-color: white; font-weight: bold"></i> Data Berhasil Disimpan
</div>
</div>
</div>
</div>
</div>

<div class="container-fluid result hidden">
<div class="row">
<div class="col-lg-12">
<div class="card">
<div class="card-body" style="color: white; font-weight: bold">
<i class='fa fa-check-square' style="font-color: white; font-weight: bold"></i> Hasil Pencarian
<table class="table table-data3 table-striped">
<thead>
<tr><th>Nama Pemohon</th> 
<th>Jenis Layanan</th> 
<th>Tgl Permohonan</th> 
<th>Status</th></tr>
</thead>
<tbody id="dt"></tbody>
</table>
</div>
</div>
</div>
</div>
</div>

<div class="container-fluid main hidden">
<div class="row fluids">
<div class="col-lg-12">
<div class="card">
<div class="card-header overview-item--cwt"><h4 style="color: #ffffff; text-shadow: rgba(0,0,0,0.5) -1px 0, rgba(0,0,0,0.3) 0 -1px, rgb(66 114 215) 0 1px, rgba(0,0,0,0.3) -1px -2px;">
FORMULIR PERMOHONAN LAYANAN BALAI&nbsp;BAHASA&nbsp;BALI</h4></div>
<div class="card-body">
<div class="card-title">
<h3 class="text-center title-2">FORMULIR PERMOHONAN LAYANAN BALAI&nbsp;BAHASA&nbsp;BALI</h3>
</div>
<hr>
<form action="#" method="post" novalidate="novalidate">
<div class="form-group">
<label for="cc-payment" class="control-label mb-1">Jenis Layanan</label> <span class="badge badge-danger">required</span><br/>
<select id="form-layan" class="form-control checked">
<option>--silakan pilih--</option>
</select>
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Nama Pemohon</label> <span class="badge badge-danger">required</span>
<input id="form-nama" type="text" class="form-control checked" /> 
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Nama Instansi</label> <span class="badge badge-danger">required</span>
<input id="form-instansi" type="text" class="form-control checked" /> 
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Alamat (Jalan)</label> <span class="badge badge-danger"></span>
<input id="form-jalan" type="text" class="form-control" /> 
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Alamat (Kota/Kabupaten</label> <span class="badge badge-danger"></span>
<input id="form-kab" type="text" class="form-control" /> 
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Telepon</label> <span class="badge badge-danger"></span>
<input id="form-telp" type="text" class="form-control" /> 
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Ponsel</label> <span class="badge badge-danger">required</span>
<input id="form-hp" type="text" class="form-control checked" /> 
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Posel</label> <span class="badge badge-danger">required</span>
<input id="form-email" type="text" class="form-control checked" /> 
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Keterangan</label> <span class="badge badge-danger">required</span>
<textarea class="form-control checked" id="form-ket"></textarea> 
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">File Surat Permohonan</label> <span class="badge badge-danger">required</span> 
<input type="file" class="form-control checked" id="srt" onchange="baca_file(this.id)"/>
<input type="hidden" id="form-srt" class="clear" />
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">File Berkas Permohonan</label> <span class="badge badge-danger"></span> 
<input type="file" class="form-control clear" id="berkas" onchange="baca_file(this.id)"/>
<input type="hidden" id="form-berkas" class="clear" />
</div>
<div class="form-group">
<label for="cc-name" class="control-label mb-1">Gambar Tandatangan (pergunakan mouse)</label> <span class="badge badge-danger">required</span> 
<div class="canvas-sign"></div>
</div>
<div class="form-group">
<button id="payment-button" type="button" class="btn btn-lg btn-info">
<span id="form-simpan" onclick="formSimpan()">Simpan</span> 
</button>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="canvas-sign-container main hidden">
<div id="bcPaint"></div> 
</div>

<script>

async function about(){
    $(".awaiting").show();
    $(".hamburger").removeClass("is-active");
          $(".navbar-mobile").hide();
    if($(".save").hasClass("hidden")){} else {$(".save").addClass("hidden")}
    if($(".check").hasClass("hidden")){} else {$(".check").addClass("hidden")}
    if($(".result").hasClass("hidden")){} else {$(".result").addClass("hidden")}
    if($(".front").hasClass("hidden")){} else {$(".front").addClass("hidden")}
    if($(".main").hasClass("hidden")){} else {$(".main").addClass("hidden")}
    if($(".cont").hasClass("hidden")){} else {$(".cont").addClass("hidden")}
    $(".cond").removeClass("hidden");
    $(".position").html("Syarat & Ketentuan");

    let cu = await fetch("<?php echo $base_url; ?>setting.json/setting-bin/all");
    let cus = await cu.json();
    if(cu.ok){
        $(".awaiting").hide();
        $("#data-cond").html(cus.rows[0].isi);
    }
}

async function contact(){
    $(".awaiting").show();
    $(".hamburger").removeClass("is-active");
          $(".navbar-mobile").hide();
    if($(".save").hasClass("hidden")){} else {$(".save").addClass("hidden")}
    if($(".check").hasClass("hidden")){} else {$(".check").addClass("hidden")}
    if($(".result").hasClass("hidden")){} else {$(".result").addClass("hidden")}
    if($(".front").hasClass("hidden")){} else {$(".front").addClass("hidden")}
    if($(".main").hasClass("hidden")){} else {$(".main").addClass("hidden")}
    if($(".cond").hasClass("hidden")){} else {$(".cond").addClass("hidden")}
    $(".cont").removeClass("hidden");
    $(".position").html("Hubungi Kami");

    let cu = await fetch("<?php echo $base_url; ?>setting.json/setting-bin/all");
    let cus = await cu.json();
    if(cu.ok){
        $(".awaiting").hide();
        $("#data-cont").html(cus.rows[1].isi);
    }
}

function jump_cari(){
    document.getElementById("q").focus();
    $(".hamburger").removeClass("is-active");
          $(".navbar-mobile").hide();
}

function gotos(){
    $(".hamburger").removeClass("is-active");
          $(".navbar-mobile").hide();
    if($(".save").hasClass("hidden")){} else {$(".save").addClass("hidden")}
    if($(".check").hasClass("hidden")){} else {$(".check").addClass("hidden")}
    if($(".result").hasClass("hidden")){} else {$(".result").addClass("hidden")}
    if($(".front").hasClass("hidden")){} else {$(".front").addClass("hidden")}
    if($(".cont").hasClass("hidden")){} else {$(".cont").addClass("hidden")}
    if($(".cond").hasClass("hidden")){} else {$(".cond").addClass("hidden")}
    $(".main").removeClass("hidden");
    $(".position").html("Formulir Permohonan");
}

async function carkat(){
    $(".removed").remove();
    $("#dt").html("<tr class='removed'><td><i class='fa fa-spinner fa-pulse fa-fw'></i> mencari data...</td></tr>");
    $(".hamburger").removeClass("is-active");
          $(".navbar-mobile").hide();

    if($(".save").hasClass("hidden")){} else {$(".save").addClass("hidden")}
    if($(".check").hasClass("hidden")){} else {$(".check").addClass("hidden")}
    if($(".main").hasClass("hidden")){} else {$(".main").addClass("hidden")}
    if($(".front").hasClass("hidden")){} else {$(".front").addClass("hidden")}
    if($(".cont").hasClass("hidden")){} else {$(".cont").addClass("hidden")}
    if($(".cond").hasClass("hidden")){} else {$(".cond").addClass("hidden")}
    $(".result").removeClass("hidden");

    let q = $("#q").val();
    let url =  "<?php echo $base_url; ?>form.json/form-bin/cari/" + q;
    let ah = await fetch(url);
    let d = await ah.json();
    if(ah.ok){
        let k = "";
        for(let i in d.rows){
            let status = "";
            let badge = "";
            if(d.rows[i].status == "1"){
                status = "ditolak"; badge = "badge-danger";
            } else {
                if(d.rows[i].konfirm == null){
                    status = "pending"; badge = "badge-warning";
                } else {
                    status = "disetujui"; badge = "badge-success";
                }
            }
            let dst = new Date(d.rows[i].tgl);
            let ds = new Intl.DateTimeFormat('id-ID').format(dst);
            k += "<tr class='removed'><td>" + d.rows[i].nama + "</td><td>" + d.rows[i].lyn + "</td><td>" + ds + "</td><td> <span class='badge " + badge + "'>" + status + "</span></td></tr>";
        }
        if(d.rows.length == 0){
            $("#dt").html("<tr class='removed'><td>tidak ada data</td></tr>");
        } else {
            $("#dt").html(k);
        }
        
    }
    $(".position").html("Pencarian Formulir Permohonan");
}

async function baca_file(f){ 
    let file = document.getElementById(f).files[0];
    let fs = new FileReader();
    fs.onload = function(evt){
        if(fs.readyState == 2){
            $("#form-" + f).val(evt.target.result);
        }
    }
    fs.readAsDataURL(file);
}

async function formSimpan(){
    let fd = new FormData();
    $("#form-simpan").attr("disabled",true);
    let tick = 0;
    let chk = document.getElementsByClassName("checked");
    for(let i in chk){
        if(chk[i].value == ""){ tick++; chk[i].focus(); alert("ada isian belum dilengkapi"); break;  }
    }
    if(tick == 0){
        $("#form-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menyimpan data...");
    let layanan = $("#form-layan").val();
    let nama = $("#form-nama").val();
    let instansi = $("#form-instansi").val();
    let jalan = $("#form-jalan").val();
    let kab = $("#form-kab").val();
    let telp = $("#form-telp").val();
    let hp = $("#form-hp").val();
    let email = $("#form-email").val();
    let ket = $("#form-ket").val();
    let f1 = document.getElementById("srt").files[0];
    let f2 = document.getElementById("berkas").files[0];
    let ttd = document.getElementById("bcPaintCanvas").toDataURL('image/png');
    let srt = $("#form-srt").val();
    let berkas = $("#form-berkas").val();
    let fd = new FormData();
    fd.append("layanan",layanan);
    fd.append("nama",nama);
    fd.append("instansi",instansi);
    fd.append("jalan",jalan);
    fd.append("kab",kab);
    fd.append("telp",telp);
    fd.append("hp",hp);
    fd.append("email",email);
    fd.append("ket",ket);
    fd.append("srt",srt);
    fd.append("berkas",berkas);
    fd.append("ttd",ttd);

    let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/add",{
        method: "POST",
        body: fd
    });
    if(ah.ok){
        $(".main").addClass("hidden");
        $(".save").removeClass("hidden");
        $("#form-simpan").html("Simpan");
        $("#form-simpan").attr("disabled",false);
        $(".checked").val("");
        $(".clear").val("");
        $.fn.bcPaint.clearCanvas();
        fetch("<?php echo $base_url; ?>form.json/form-bin/notif_email");
    } else {
        console.log(ah.status);
    }
    }
    
}



$(document).ready(function(){


    async function set_desc(){
   let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/imagery");
    let btp = await ah.json();
    if(ah.ok){ 
        $(".imagery").attr("src","<?php echo $logo_url; ?>view/images/icon/" + btp.rows[0].img);
    } 
}
set_desc();

async function notif(){
    $(".check").addClass("hidden"); 
    let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/stat");
    let koh = await ah.json();
    if(ah.ok){
        $(".tot").html(koh.total);
        $(".week").html(koh.minggu);
        $(".sls").html(koh.selesai);
        $(".prs").html(koh.proses);
    }
}

function atur_layanan(){
    $(".remove").remove();
    let cache = JSON.parse(localStorage.getItem("layanans"));
    let k = "";
    for(let i in cache.rows){
        k += "<option class='remove' value='" + cache.rows[i].id + "'>" + cache.rows[i].nama + "</option>";
    }
    $("#form-layan").append(k);
    $(".front").removeClass("hidden");
   notif();
}    

async function get_cache(){
 
let wkt_update = <?php 
    
    $d = date("G");
    if($d % 2 == 0){
        echo "true";
    } else {
        echo "false";
    }
    
    ?>;

let cache = localStorage.getItem("layanans");
if(wkt_update){
    //update cache
    let c = await fetch("<?php echo $base_url; ?>cache.json/cache-bin/update");
    let ca = await c.text();
    if(c.ok){
        localStorage.removeItem("layanans");
        localStorage.setItem("layanans",ca);
        atur_layanan();
    }
} else {

if(typeof cache === "object"){
    //update cache
    let c = await fetch("<?php echo $base_url; ?>cache.json/cache-bin/update");
    let ca = await c.text();
    if(c.ok){
        localStorage.removeItem("layanans");
        localStorage.setItem("layanans",ca);
        atur_layanan();
    }
} else {
    atur_layanan();
}

}

}

get_cache();
 

});
</script>

<!-- canvas-sign library --> 


<link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>view/canvas-sign/css/bcPaint.css">
		<link type="text/css" rel="stylesheet" href="<?php echo $base_url; ?>view/canvas-sign/css/bcPaint.mobile.css"> 
		<script type="text/javascript" src="<?php echo $base_url; ?>view/canvas-sign/js/bcPaint.js"></script>

<script>
$('#bcPaint').bcPaint();
</script>
<!-- canvas-sign library -->

<script src="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/popper.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/bootstrap.min.js" type="text/javascript"></script>

<script src="<?php echo $base_url; ?>view/vendor/slick/slick.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/wow/wow.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/animsition/animsition.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/counter-up/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/counter-up/jquery.counterup.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/circle-progress/circle-progress.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/perfect-scrollbar/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/chartjs/Chart.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/select2/select2.min.js" type="text/javascript">
    </script>

<script src="<?php echo $base_url; ?>view/js/main.js" type="text/javascript"></script>
</body>

<!-- Mirrored from colorlib.com/polygon/cooladmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 03:41:59 GMT -->
</html>