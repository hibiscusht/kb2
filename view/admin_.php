<?php
require "../mod/config/conf.php";
?>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from colorlib.com/polygon/cooladmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 03:41:05 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="au theme template">
<meta name="author" content="Hau Nguyen">
<meta name="keywords" content="au theme template">

<title>Zoho Forms</title>

<link href="<?php echo $base_url; ?>view/css/font-face.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/wow/animate.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/slick/slick.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/select2/select2.min.css" rel="stylesheet" media="all">
<link href="<?php echo $base_url; ?>view/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

<link href="<?php echo $base_url; ?>view/css/theme.css" rel="stylesheet" media="all">

<script src="<?php echo $base_url; ?>view/vendor/jquery-3.2.1.min.js" type="text/javascript"></script>

</head>
<body class="animsition">

<?php
$det = explode("/",$_GET["details"]); 
switch($det[0]){ 

    case "home": ?> 
    
    <div class="page-wrapper">
    <style>
        .lock {opacity: 0}
        .overview-item--cwt {
        background-image: -webkit-linear-gradient(90deg, #4272d7 0%, #4272d7 100%);}
        .transparent {background: rgba(0,0,0,0)}  
        .blue {background: #4272d7 !important}
        .hidden {display: none}
        #my-chart {width: 885px !important; height: 150px !important}
        @media (max-width: 992px){ 
            .bluish {background: white !important}
            #my-chart {width: 379px !important; height: 162px !important}
            .hamburger {background: rgba(0,0,0,0) !important}
            .lefty {left: 20px}
        }
    </style>
<div class="lock" id="locked" data-token="<?php echo $det[1]; ?>">
<header class="header-mobile blue d-block d-lg-none">
<div class="header-mobile__bar">
<div class="container-fluid">
<div class="header-mobile-inner">
<a class="logo" href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>">
<img style="margin-inline-start: 25px;" src="" alt="@hibiscusht" class="imagery">
</a>
<button style="align-items: flex-end;
    float: right;
    margin-left: 30%;
    padding-top: 1%;
    margin-right: 5%; ">&nbsp;&nbsp;</button>
<button class="hamburger hamburger--slider" type="button">
<span class="hamburger-box">
<span class="hamburger-inner"></span>
</span>
</button>
</div>
</div>
</div>
<nav class="navbar-mobile">
<div class="container-fluid">
<ul class="navbar-mobile__list list-unstyled modules mobile"></ul>
</div>
</nav>
</header>


<aside class="menu-sidebar d-none d-lg-block">
<div class="logo blue">
<a href="<?php echo $base_url; ?>admin/home/<?php echo $det[1]; ?>">
<img style="" src="" alt="@hibiscusht" class="imagery">
</a>
</div>
<div class="menu-sidebar__content js-scrollbar1 ps">
<nav class="navbar-sidebar">
<ul class="list-unstyled navbar__list modules"></ul>
</nav>
 <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
</aside>


<div class="page-container">

<header class="header-desktop blue bluish">
<div class="section__content section__content--p30">
<div class="container-fluid">
<div class="header-wrap">
<form class="form-header" action="#" method="POST">
<input class="au-input au-input--xl" type="text" id="q" name="search" placeholder="Cari permohonan" />
<button class="au-btn--submit" type="button" onclick="carkat()">
<i class="zmdi zmdi-search"></i>
</button>
</form>
<div class="header-button">
<div class="noti-wrap">
<div class="noti__item js-item-menu"  onclick="clrnotif('komen-i')">
<i class="zmdi zmdi-email"></i>
<span class="quantity" id="komen-i"><i class="fa fa-spinner fa-pulse fa-fw text-white" style="font-size: 11.5px; "></i></span>
<div class="email-dropdown js-dropdown lefty">
<div class="email__title">
<p>Ajuan Permohonan</p>
</div>
<div class="email__item"  style="height: 90px; overflow-y: auto"> 
<div id="komen" style="height: 100%">
<div class="content progress"> 
<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100%; background-color: #b8bcbf !important" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" ></div>
</div>
</div>
</div>
<div class="email__footer">
<a href="#" onclick="unhides('permohonan-table')">Lihat Semua</a>
</div>
</div>
</div>
<!-- end -->
</div>
<div class="account-wrap">
<div class="account-item clearfix js-item-menu">
<div class="image">
<img class="profilpic" src="<?php echo $base_url; ?>view/images/icon/avatar-01.jpg" alt="John Doe" />
</div>
<div class="content">
<a class="js-acc-btn username" href="#"> </a>
</div>
<div class="account-dropdown js-dropdown">
<div class="info clearfix">
<div class="image">
<a href="#">
<img class="profilpic" src="<?php echo $base_url; ?>view/images/icon/avatar-01.jpg"  alt="John Doe" />
</a>
</div>
<div class="content">
<h5 class="name">
<a href="#" class="username"> </a>
</h5>
<span class="email"></span>
</div>
</div>
<div class="account-dropdown__body">
</div>
<div class="account-dropdown__footer">
<a href="<?php echo $base_url; ?>admin/logout/<?php echo $det[1]; ?>">
<i class="zmdi zmdi-power"></i>Logout</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header>


<div class="main-content">
<div class="section__content section__content--p30">


<div class="row global home">
<!-- banner 1 -->

<div class="col-sm-6 col-lg-3">
<div class="overview-item overview-item--c1">
<div class="overview__inner">
<div class="overview-box clearfix">
<div class="icon">
<i class="zmdi zmdi-account-o"></i>
</div>
<div class="text">
<h2 id="tot">0</h2>
<span>permohonan total</span>
</div>
</div> 
</div>
</div>
</div>

<!-- banner 1 -->

<!-- banner 2 -->

<div class="col-sm-6 col-lg-3">
<div class="overview-item overview-item--c2">
<div class="overview__inner">
<div class="overview-box clearfix">
<div class="icon">
<i class="zmdi zmdi-account-o"></i>
</div>
<div class="text">
<h2 id="bln">0</h2>
<span>permohonan bulan ini</span>
</div>
</div> 
</div>
</div>
</div>

<!-- banner 2 -->
  

<!-- banner 3 -->

<div class="col-sm-6 col-lg-3">
<div class="overview-item overview-item--c3">
<div class="overview__inner">
<div class="overview-box clearfix">
<div class="icon">
<i class="zmdi zmdi-account-o"></i>
</div>
<div class="text">
<h2 id="hari">0</h2>
<span>permohonan hari ini</span>
</div>
</div> 
</div>
</div>
</div>

<!-- banner 3 -->


</div>

<!--- module user -->
<!-- tabel --> 

<div class="global hidden" id="user-table">

<div class="col-lg-12" style="margin-bottom: 5%; background: white; padding: 2%">
<div class="card-body">
<button type="button" class="btn btn-primary btn-sm" onclick="cycles('user-table','user-input')">
<i class="fa fa-plus-circle"></i>&nbsp; Tambah Admin</button>
</div></div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Admin Kamus</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>aksi</th>
<th>nama</th>
<th>status</th> 
</tr>
</thead>
<tbody id="u-dt">
</tbody>
</table>
</div>
</div>
</div>
</div>

</div>
<!-- input -->

<div class="row global hidden" id="user-input">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Admin Kamus</h3>
<input type="hidden" id="user-edit" class="clear"/>
 
<div class="form-group">
<label for="user" class="control-label mb-1">Username</label>
<input id="user" type="text" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-pwd" class="control-label mb-1">Password <span class="cek_edit"></span></label>
<input id="user-pwd" type="password" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-sta" class="control-label mb-1">Status</label>
<select class="clear form-control" id="user-sta">
<option value="">--silakan pilih--</option>
<option value="0">Tidak Aktif</option>
<option value="1">Aktif</option>
</select>
</div>

<div class="form-group">
<label for="user-name" class="control-label mb-1">Nama Lengkap</label>
<input id="user-name" type="text" class="clear form-control" />
</div>

<div class="form-group">
<label for="user-pic" class="control-label mb-1">Profile Pic <span class="cek_edit"></span></label>
<input id="user-pic" type="file" class="clear form-control" />
</div>

<div>
<button type="button" class="btn btn-sm btn-info">
<span id="user-simpan" onclick="userSimpan()">
<i class="fa fa-lock fa-lg"></i>&nbsp;Simpan</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('user-input','user-table')">
<i class="fa fa-lock fa-lg"></i>&nbsp;
<span>Batal</span>
</button>
</div>
</div>
</div>
</div>

<!--- module user -->

<!-- jenis layanan -->

<div class="global hidden" id="layanan-table">

<div class="col-lg-12" style="margin-bottom: 5%; background: white; padding: 2%">
<div class="card-body">
<button type="button" class="btn btn-primary btn-sm" onclick="cycles('layanan-table','layanan-input')">
<i class="fa fa-plus-circle"></i>&nbsp; Tambah Jenis Layanan</button>
</div></div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Jenis Layanan</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>aksi</th>
<th>Jenis Layanan</th>
</tr>
</thead>
<tbody id="lyn-dt"> </tbody>
</table>
</div>
</div>
</div>
</div>

</div>


<!-- input --> 

<div class="row global hidden" id="layanan-input">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Jenis Layanan</h3>
<input type="hidden" id="lyn-edit" class="clear">
<div class="form-group">
<label for="lyn" class="control-label mb-1">Jenis Layanan</label>
<input id="lyn" type="text" class="clear form-control">
</div>
<div>
<button type="button" class="btn btn-sm btn-info"> 
<span id="lyn-simpan" onclick="lynSimpan()">Simpan</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('layanan-input','layanan-table')"> 
<span>Batal</span>
</button>
</div>
</div>
</div>
</div>

<!-- jenis layanan -->


<!-- permohonan -->

<div class="global hidden" id="permohonan-table">

<div class="col-lg-12" style="margin-bottom: 5%; background: white; padding: 2%">
<div class="card-body"> 
</div></div>

<div class="row">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Permohonan</h3>
<div class="table-responsive table--no-card m-b-30">
<table class="table table-borderless table-striped table-earning table-data">
<thead>
<tr>
<th>aksi</th>
<th>Jenis Layanan</th>
<th>nama pemohon</th>
</tr>
</thead>
<tbody id="mhn-dt"> </tbody>
</table>
</div>
</div>
</div>
</div>

</div>


<!-- input --> 

<div class="row global hidden" id="permohonan-input">
<div class="col-lg-12">
<div class="top-campaign">
<h3 class="title-3 m-b-30">Balas Konfirmasi</h3>
<input type="hidden" id="konfirm-edit" class="clear">
<input type="hidden" id="konfirm-layanan" class="clear">
<div class="form-group">
<label for="lyn" class="control-label mb-1">Jenis Layanan</label>
<div id="konfirm-jns"></div>
</div> 
<div class="form-group">
<label for="lyn" class="control-label mb-1">Nama Pemohon</label>
<div id="konfirm-pemohon"></div>
</div>
<div class="form-group">
<label for="lyn" class="control-label mb-1">Pesan</label>
<textarea id="konfirm" class="form-control"></textarea>
</div> 
<div class="form-group">
<label for="lyn" class="control-label mb-1">Lampiran</label> 
<input type="file" />
</div> 
<div>
<button type="button" class="btn btn-sm btn-info"> 
<span id="konfirm-simpan" onclick="konfirmSimpan()">Kirim</span>
</button>
<button type="button" class="btn btn-sm btn-danger" onclick="cycles('permohonan-input','permohonan-table')"> 
<span>Batal</span>
</button>
</div>
</div>
</div>
</div>

<!-- permohonan -->



</div>
<!-- section content berakhir di sini -->
</div>
<!-- container utama berakhir di sini -->

</div>
    
    </div>
    </div>

    <script>

async function carkats(){
    let q = $("#q").val();
    let id = "permohonan-table";
    let url =  "<?php echo $base_url; ?>form.json/form-bin/cari/" + q;
    let ah = {ok:false};
    let btp = "";

    let cache = localStorage.getItem(id);
    if(typeof cache === "object"){
         //update cache
    let c = await fetch(url);
    let ca = await c.text();
    if(c.ok){
        localStorage.removeItem(id);
        localStorage.setItem(id,ca);
        ah = {ok:true};
        btp = JSON.parse(localStorage.getItem(id));
    }
    
    //console.log("abc");
    } else {
        let wkt_update = <?php 
    
    $d = date("G");
    if($d % 2 == 0){
        echo "true";
    } else {
        echo "false";
    }
    
    ?>;

    if(wkt_update){
         //update cache
    let c = await fetch(url);
    let ca = await c.text();
    if(c.ok){
        localStorage.removeItem(id);
        localStorage.setItem(id,ca);
        ah = {ok:true};
        btp = JSON.parse(localStorage.getItem(id));
    }
    } else {
        ah = {ok:true};
        btp = JSON.parse(localStorage.getItem(id));
    }
   // console.log("abcd");
    
    }

   
    $(".removed").remove();
    $("#mhn-dt").append("<tr class='removed'><td colspan='2'><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</td></tr>");
    if(ah.ok){
        let jk = ""; 
        if(btp.rows.length == 0){
            jk = "<tr class='removed'><td colspan=2>tidak ada data</td></tr>";
        } else {
            for(let i in btp.rows){
                let aksi = "";
             if(btp.rows[i].konfirm == null){
                if(btp.rows[i].status == null){
                    aksi = "<button id='" + btp.rows[i].id + "' class='btn btn-sm btn-info' onclick='formTerima(this.id)' data-type='entry'>Terima</button>&nbsp;<button id='tlk-" + btp.rows[i].id + "' class='btn btn-sm btn-danger' onclick='formTolak(this.id)' data-type='entry'>Tolak</button>";
                } else {
                    aksi = "<span class='badge badge-danger'>ditolak</span>";
                }
             } else {
                aksi = "<button id='" + btp.rows[i].id + "' class='btn btn-sm btn-info' onclick='formTerima(this.id)' >Kirim Ulang Konfirmasi</button>";
             } 
            jk += "<tr class='removed'><td>" + aksi + "</td><td>" + btp.rows[i].lyn + "</td><td>" + btp.rows[i].nama + "</td></tr>";
            } 
        }  
        $(".removed").remove();
        $("#mhn-dt").append(jk);
    }
   
}

function carkat(){
    $(".global").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    }); 
          $("#permohonan-table").removeClass("hidden");
    carkats();
}

    
function clrnotif(id){
    $("#" + id).css("opacity","0");
}

function cycles(a,b){
    $("#" + b).removeClass("hidden");
    $("#" + a).addClass("hidden");
    $(".clear").val("");  
    
}

async function get_data(id,chk){
    let cache = JSON.parse(localStorage.getItem(id));
    switch(id){
        case "layanan-table": 
        $(".removed").remove();
        let k = "";
        for(let i in cache.rows){
            k += "<tr class='removed'><td><button class='btn btn-sm btn-danger' id='del-" + cache.rows[i].id + "' onclick='del(this.id)' data-type='layanan-table'>Hapus</button> <button class='btn btn-sm btn-info' id='edit-" + cache.rows[i].id + "' onclick='edit(this.id)' data-type='layanan'>Edit</button></td><td>" + cache.rows[i].nama + "</td></tr>";
        }
        $("#" + chk + "-dt").append(k);
         break;
         case "permohonan-table":
         $(".removed").remove();
         let jk = "";
         for(let i in cache.rows){
             let aksi = "";
             if(cache.rows[i].konfirm == null){
                if(cache.rows[i].status == null){
                    aksi = "<button id='" + cache.rows[i].id + "' class='btn btn-sm btn-info' onclick='formTerima(this.id)' data-type='entry'>Terima</button>&nbsp;<button id='tlk-" + cache.rows[i].id + "' class='btn btn-sm btn-danger' onclick='formTolak(this.id)' data-type='entry'>Tolak</button>";
                } else {
                    aksi = "<span class='badge badge-danger'>ditolak</span>";
                }
             } else {
                aksi = "<button id='" + cache.rows[i].id + "' class='btn btn-sm btn-info' onclick='formTerima(this.id)' >Kirim Ulang Konfirmasi</button>";
             } 
            jk += "<tr class='removed'><td>" + aksi + "</td><td>" + cache.rows[i].lyn + "</td><td>" + cache.rows[i].nama + "</td></tr>";
         }
         $("#" + chk + "-dt").append(jk);
         break;
         case "user-table":
         $(".removed").remove();
         let jki = "";
         for(let i in cache.rows){

             let status = (cache.rows[i].status == 1)? "Aktif" : "Tidak Aktif";
            
            jki += "<tr class='removed'><td><button class='btn btn-sm btn-danger' id='del-" + cache.rows[i].id + "' onclick='del(this.id)' data-type='user-table'>Hapus</button> <button class='btn btn-sm btn-info' id='edit-" + cache.rows[i].id + "' onclick='edit(this.id)' data-type='user'>Edit</button></td></td><td>" + cache.rows[i].nama + "</td><td>" + status + "</td></tr>";
         }
         $("#" + chk + "-dt").append(jki);
         break;
    }
}

async function del(id){
    let ids = id.split("-");
    let types = $("#" + id).data("type");
    let ask = confirm("apakah yakin menghapus?");
    if(ask){
        $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menghapus data...");
        if(types == "layanan-table"){
            let c = await fetch("<?php echo $base_url; ?>layanan.json/layanan-bin/delete/<?php echo $det[1]; ?>/" + ids[1]);
        if(c.ok){
            localStorage.removeItem(types);
            populate(types);
        }
        } else if(types == "user-table"){
            let c = await fetch("<?php echo $base_url; ?>user.json/user-bin/delete/<?php echo $det[1]; ?>/" + ids[1]);
        if(c.ok){
            localStorage.removeItem(types);
            populate(types);
        }
        }
        
    }
}

async function edit(id){
    let ids = id.split("-");
    let types = $("#" + id).data("type"); 
        $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...");
        if(types == "layanan"){
            let c = await fetch("<?php echo $base_url; ?>layanan.json/layanan-bin/gets2/" + ids[1]);
            let cu = await c.json();
        if(c.ok){ 
            cycles(types + "-table",types + "-input");
            $("#lyn-edit").val(ids[1]);
            $("#lyn").val(cu.rows[0].nama);
        }
        } else if(types == "user"){
            let c = await fetch("<?php echo $base_url; ?>user.json/user-bin/edituser/" + ids[1]);
            let cu = await c.json();
        if(c.ok){ 
            cycles(types + "-table",types + "-input");
            $(".cek_edit").html("(kosongkan jika tidak diubah)");
            $("#user-edit").val(ids[1]);
            $("#user").val(cu.rows[0].username);
                $("#user-sta").val(cu.rows[0].status);
                $("#user-name").val(cu.rows[0].nama);
        }

        }
        $("#" + id).html("Edit");
}

async function populate(id){
    let chk = "";
    let url = "<?php echo $base_url; ?>";
    switch(id){
        case "layanan-table": chk = "lyn"; url += "layanan.json/layanan-bin/gets"; break;
        case "permohonan-table": chk = "mhn"; url += "form.json/form-bin/getsall"; break;
        case "user-table": chk = "u"; url += "user.json/user-bin/getsall"; break;
    }
    $("#" + chk + "-dt").append("<tr class='removed'><td><i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...</td></tr>");
    let cache = localStorage.getItem(id);
    if(typeof cache === "object"){
         //update cache
    let c = await fetch(url);
    let ca = await c.text();
    if(c.ok){
        localStorage.removeItem(id);
        localStorage.setItem(id,ca);
        get_data(id,chk);
    }
    
    //console.log("abc");
    } else {
        let wkt_update = <?php 
    
    $d = date("G");
    if($d % 2 == 0){
        echo "true";
    } else {
        echo "false";
    }
    
    ?>;

    if(wkt_update){
         //update cache
    let c = await fetch(url);
    let ca = await c.text();
    if(c.ok){
        localStorage.removeItem(id);
        localStorage.setItem(id,ca);
       get_data(id,chk);
    }
    } else {
       get_data(id,chk);
    }
   // console.log("abcd");
    }
}

/* fungsi spesifik */

async function formTolak(id){
    let ids = id.split("-");
    let ask = confirm("menolak permohonan akan menyebabkan permohonan yang ditolak tidak dapat diakses. lanjutkan?");
    if(ask){
        $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memproses data...");
        let k = await fetch("<?php echo $base_url; ?>konfirmasi.json/konfirmasi-bin/reject/<?php echo $det[1]; ?>/" + ids[1]);
        if(k.ok){ 
            localStorage.removeItem("permohonan-table");
            populate("permohonan-table");
        }
    }
}

async function formTerima(id){
    $("#" + id).html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memuat data...");
    cycles("permohonan-table","permohonan-input");
    $("#konfirm-layanan").val(id);
    let j = await fetch("<?php echo $base_url; ?>form.json/form-bin/edits/" + id);
    let x = await j.json();
    if(j.ok){
        $("#konfirm-jns").html(x.rows[0].lyn);
        $("#konfirm-pemohon").html(x.rows[0].nama);
        let konfirm_id = (x.rows[0].konfirm == null)? 0 : x.rows[0].konfirm; 
        let ah = await fetch("<?php echo $base_url; ?>konfirmasi.json/konfirmasi-bin/gets/" + konfirm_id);
        let btp = await ah.json(); 
        let ret = "Terima";
        if(konfirm_id == 0){
            //kosong
        } else {
            if(ah.ok){
                $("#konfirm").val(btp.rows[0].ket);
                $("#konfirm-edit").val(btp.rows[0].id);
                ret = "Kirim Ulang Konfirmasi";
            }
           
        } 
        $("#" + id).html(ret);
    }
    
    
}

async function konfirmSimpan(){
    $("#konfirm-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> mengirim...");
    let konfirm = $("#konfirm-edit").val();
    let lyn = $("#konfirm-layanan").val();
    let ket = $("#konfirm").val();
    let fd = new FormData();
    if(konfirm == ""){
        //insert
        fd.append("lyn",lyn);
        fd.append("ket",ket);
        let j = await fetch("<?php echo $base_url; ?>konfirmasi.json/konfirmasi-bin/add/<?php echo $det[1]; ?>",{
            method: "POST",
            body: fd
        });
        if(j.ok){
            $("#konfirm-simpan").html("Kirim");
            cycles("permohonan-input","permohonan-table");
        }
    } else {
        //update 
        fd.append("ket",ket);
        fd.append("konfirm",konfirm);
        let j = await fetch("<?php echo $base_url; ?>konfirmasi.json/konfirmasi-bin/update/<?php echo $det[1]; ?>",{
            method: "POST",
            body: fd
        });
        if(j.ok){
            $("#konfirm-simpan").html("Kirim");
            cycles("permohonan-input","permohonan-table");
        }
    }
    localStorage.removeItem("permohonan-table");
    populate("permohonan-table");
}

async function lynSimpan(){
    $("#lyn-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menyimpan data...");
    let lyn = $("#lyn").val();
    let edt = $("#lyn-edit").val();
    let fd = new FormData();
    if(edt == ""){
        //insert
        fd.append("lyn",lyn);
        let c = await fetch("<?php echo $base_url; ?>layanan.json/layanan-bin/add/<?php echo $det[1]; ?>",{
            method: "POST",
            body: fd
        });
        if(c.ok){
            localStorage.removeItem("layanan-table");
            populate("layanan-table");
            cycles("layanan-input","layanan-table");
            $("#lyn-simpan").html("Simpan");
        }
    } else {
        //update
        fd.append("lyn",lyn);
        fd.append("lyn_id",edt);
        let c = await fetch("<?php echo $base_url; ?>layanan.json/layanan-bin/update/<?php echo $det[1]; ?>",{
            method: "POST",
            body: fd
        });
        if(c.ok){
            localStorage.removeItem("layanan-table");
            populate("layanan-table");
            cycles("layanan-input","layanan-table");
            $("#lyn-simpan").html("Simpan");
        }
    }
}

async function userSimpan(){
    $("#user-simpan").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> menyimpan...");
    let edit = $("#user-edit").val();
    let uname = $("#user").val();
    let pwd = $("#user-pwd").val();
    let stt = $("#user-sta").val();
    let nama = $("#user-name").val(); 
    let pic = document.getElementById("user-pic").files[0]; 
    let fd = new FormData();
    if(typeof pic === "undefined"){
            fd.append("uname",uname);
            fd.append("pwd",pwd);
            fd.append("stt",stt);
            fd.append("nama",nama);
        } else {
            fd.append("uname",uname);
            fd.append("pwd",pwd);
            fd.append("stt",stt);
            fd.append("nama",nama);
            fd.append("pic",pic);
        }
    if(edit == ""){
        //insert
        let a = await fetch("<?php echo $base_url;?>user.json/user-bin/add/<?php echo $det[1] ?>",{
            method: "POST",
            body: fd
        });
        if(a.ok){
            $("#user-simpan").html("<i class='fa fa-lock fa-lg'></i>&nbsp;Simpan");
            cycles("user-input","user-table");
            populate("user-table");
        }  
    } else {
        fd.append("edit",edit);
        if(typeof pic === "undefined"){
            fd.append("uname",uname);
            fd.append("pwd",pwd);
            fd.append("stt",stt);
            fd.append("nama",nama);
        } else {
            fd.append("uname",uname);
            fd.append("pwd",pwd);
            fd.append("stt",stt);
            fd.append("nama",nama);
            fd.append("pic",pic);
        }
        //update
        let a = await fetch("<?php echo $base_url;?>user.json/user-bin/update/<?php echo $det[1] ?>",{
            method: "POST",
            body: fd
        });
        if(a.ok){
            $("#user-simpan").html("<i class='fa fa-lock fa-lg'></i>&nbsp;Simpan");
            cycles("user-input","user-table");
            populate("user-table");
        }  
    }
}

/* fungsi spesifik */

async function unhides(id){
    $(".global").each(function(a,b){
        if($($(this)).hasClass("hidden")){} else {
            $($(this)).addClass("hidden");
        }
    });
         
          $(".hamburger").removeClass("is-active");
          $(".navbar-mobile").hide();
          $("#" + id).removeClass("hidden");

           populate(id);

        }
    $(document).ready(function(){

        
async function set_desc(){
   let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/imagery");
    let btp = await ah.json();
    if(ah.ok){ 
        $(".imagery").attr("src","<?php echo $logo_url; ?>view/images/icon/" + btp.rows[0].img);
    } 
}
set_desc();

        async function jml(){
    let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/stat");
    let koh = await ah.json();
    if(ah.ok){
        $("#tot").html(koh.total);
        $("#bln").html(koh.bln);
        $("#hari").html(koh.hari);
    }
}   

jml();

        async function push_notif(){
    
    let b = await fetch("<?php echo $base_url; ?>form.json/form-bin/gets");
    let bb = await b.json();
    if(b.ok){
        $("#komen-i").html(bb.jml);
        $("#komen").html(bb.term);
    }

}    

push_notif();

        async function get_module(){
            let a = await fetch("<?php echo $base_url; ?>module.json/module-bin/gets");
            let b = await a.json();
            if(a.ok){
                let mdl = "";
                for(let i in b.rows){
                    if(b.rows[i].name == "Home"){
                        mdl += "<li><a href=''><i class='fa fa-folder'></i>" + b.rows[i].name + "</a></li>";
                    } else {
                        let lnk = '"' + b.rows[i].link + '"';
                        mdl += "<li onclick='unhides(" + lnk + ")'><a href='#'><i class='fa fa-folder'></i>" + b.rows[i].name + "</a></li>";
                    }
                    
                }
                $(".modules").append(mdl);
            }
        }
        get_module();

        async function cek_login(){
            let token = $("#locked").data("token");
            let chk = await fetch("<?php echo $base_url; ?>login.json/login-bin/check/" + token);
            let chku = await chk.json();
            if(chku.status == "login again"){
                alert("anda belum login");
                location.href="<?php echo $base_url; ?>admin/login";
            } else {
                $(".lock").css("opacity","1");
            }
        }
        cek_login();
    });
    </script>
    
    <?php
    break;
    case "login": 
    ?>

    <style>
    .header {
        position: relative;
    z-index: 1;
    background-color: #006699;
    width: 100%;
    height: 50px;
    }
    .classy-nav-container {
        position: relative;
  z-index: 100;
  background-color: #ffffff;
    }
    .classy-navbar {
        width: 100%;
  height: 70px;
  padding: 0.5em 2em;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  -ms-grid-row-align: center;
  align-items: center;
    }
    .single-service-area {
        position: relative;
  z-index: 1;
  padding: 40px 20px;
  border: 1px solid #e5e5e5;
  text-align: center;
  -webkit-transition-duration: 500ms;
  -o-transition-duration: 500ms;
  transition-duration: 500ms;
    }
    .single-service-area:hover {
        border-color: transparent;
    -webkit-box-shadow: 0 2px 40px 8px rgba(15, 15, 15, 0.15);
    box-shadow: 0 2px 40px 8px rgba(15, 15, 15, 0.15);
    }
    .login-form {
        width: 50%;
        margin: 0 25% 0 25%;
    }
    @media (max-width: 991px){
        .header {
            height: 30px;
        }
        .single-service-area {
            padding: 30px 15px;
        }
        .login-form {
        width: 90%;
        margin: 5%;
    }
    }
    </style>

<div class="page-wrapper">
<div class="page-content--bge5" style="height: 90vh !important">
<div class="header"></div>
<div class="classy-nav-container">
<div class="container">
<div class="classy-navbar">
<div style="max-width: 58px; overflow: hidden; text-indent: -5px">
<img style="max-width: 350px !important; height: 61px;" src="" alt="@hibiscusht" class="imagery"></div> SISTEM INFORMASI PERSURATAN <br/>
Balai Bahasa Provinsi Bali
</div>
</div>
</div>
<div class="container" style="padding-top: 20px">
<div class="row">
                <div class="col-12">
                    <!-- Section Heading -->
                    <div class="text-center">
                        <h2><b>Login </b></h2>
                        <p><b>Masukkan Username dan Password dengan benar</b></p>
                    </div>
                </div>
            </div> 
<div class="single-service-area">
<form action="#" method="post" class="login-form">
<div class="form-group">
<label> </label>
<input class="au-input au-input--full" type="text" id="uname" placeholder="username">
</div>
<div class="form-group">
<label> </label>
<input class="au-input au-input--full" type="password" id="pwd" placeholder="password">
</div>

<button class="au-btn au-btn--block au-btn--green m-b-20" type="button" onclick="kirim()" id="send">Login</button>
</form>
</div>
</div>
</div>
</div> 
<footer class="footer" style="background-color: #2F4F4F; color: white; padding-top: 10px;">
        <div class="container">
            <div class="row justify-content-between">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="single-footer-widget">
                        <!-- Footer Logo -->
                        <p class="mb-30" style="background-color: #2F4F4F; color: white;"></p>

                        <!-- Copywrite Text -->
                        
                    </div>
                </div>

                <!-- Single Footer Widget -->

                <!-- Single Footer Widget -->
                <div class="col-12 col-md-4 col-xl-3">
                    <div class="single-footer-widget">
                        <!-- Contact Address -->
                        <div class="contact-address">
                            <p><b style=" color: white;">BALAI BAHASA BALI</b></p>
                            <small>"...................................."</small>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </footer>
<script>
async function set_desc(){
   let ah = await fetch("<?php echo $base_url; ?>form.json/form-bin/imagery");
    let btp = await ah.json();
    if(ah.ok){ 
        $(".imagery").attr("src","<?php echo $logo_url; ?>view/images/icon/" + btp.rows[0].img);
    } 
}
set_desc();
    async function kirim(){
        $("#send").attr("disabled",true);
        $("#send").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> memvalidasi...");
        let uname = $("#uname").val();
        let pwd = $("#pwd").val();
        let hash = btoa(uname + ":" + pwd);
        let k = await fetch("<?php echo $base_url; ?>login.json/login-bin/validate/" + hash);
        let m = await k.json();
        if(m.status != "invalid"){
            $("#send").html("<i class='fa fa-spinner fa-pulse fa-fw'></i> mengalihkan...");
            location.href="<?php echo $base_url; ?>admin/home/" + m.status + "";
        } else {
            $("#send").attr("disabled",false);
            $("#send").html("Login");
            alert("username/password salah");
        }
    }
    </script>
   
    <?php
    break;
    case "logout": 
    ?>
    <script>
    async function send(){
        let hash = "<?php echo $det[1]; ?>";
        let k = await fetch("<?php echo $base_url; ?>login.json/login-bin/invalidate/" + hash);
        let j = await k.json();
        if(j.status == "login again"){
            alert("terimakasih telah menggunakan aplikasi kamus");
            location.href = "<?php echo $base_url; ?>admin/login";
        }
    }
    send();
    </script>
    <?php
    break;

}

    ?>

<script src="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/popper.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-4.1/bootstrap.min.js" type="text/javascript"></script>

<script src="<?php echo $base_url; ?>view/vendor/slick/slick.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/wow/wow.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/animsition/animsition.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/counter-up/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/counter-up/jquery.counterup.min.js" type="text/javascript">
    </script>
<script src="<?php echo $base_url; ?>view/vendor/circle-progress/circle-progress.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/perfect-scrollbar/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/chartjs/Chart.bundle.min.js" type="text/javascript"></script>
<script src="<?php echo $base_url; ?>view/vendor/select2/select2.min.js" type="text/javascript">
    </script>

<script src="<?php echo $base_url; ?>view/js/main.js" type="text/javascript"></script>
</body>

<!-- Mirrored from colorlib.com/polygon/cooladmin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 19 Aug 2020 03:41:59 GMT -->
</html>